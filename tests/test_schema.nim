
import
  preserves

type
  Foo* {.preservesRecord: "foo".} = object
    `x`*: seq[string]
    `y`*: BiggestInt
    `z`*: BiggestInt
proc `$`*(x: Foo): string =
  `$`(toPreserves(x))

proc encode*(x: Foo): seq[byte] =
  encode(toPreserves(x))
