# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

from std/os import getEnv
import
  std/sets,
  pkg/preserves,
  syndicate, syndicate/relays

proc echo(args: varargs[string, `$`]) =
  stderr.writeLine(args)

if getEnv"SYNDICATE_ROUTE" == "":
  quit()

runActor("test_dataspace") do (turn: Turn):
  resolveEnvironment(turn) do (turn: Turn, ds: Cap):
    var assertions = initHashSet[int]()

    for i in 0..31:
      let r = initRecord("foobar", i.toPreserves)
      publish(turn, ds, r)
      echo "published ", r
      assertions.incl(i)

    onPublish(turn, ds, matchRecord("foobar", grab())) do (i: int):
      echo "grabbed assertion ", i
      assertions.excl(i)
      if assertions.len == 0: quit()
