# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[monotimes, random, times, unittest],
  pkg/syndicate

const testCount = 1 shl 12

type Foobar {.preservesRecord: "foo".} = object
  n: uint64

proc `$`(f: Foobar): string = $f.toPreserves

suite "turns":
  runActor("test-turns") do (turn: Turn):
    let ds = turn.facet.newDataspace()
    linkActor(turn, "turns-observer") do (turn: Turn):
      publish(turn, ds, initRecord("test-observer"))
      var
        rand = initRand(1)
        want: Foobar
        counter: int
      let start = getMonoTime()
      onPublish(turn, ds, Foobar.matchType.grab) do (have: Foobar):
        want.n = rand.next()
        check have == want
        inc counter
        if counter == testCount:
          let stop = getMonoTime()
          echo "observation rate: ", inMicroseconds((stop - start) div counter.int64), "µs"

    block:
      var
        rand = initRand(1)
        record: Foobar
        h: Handle
        i: int

      proc bump(turn: Turn) =
        if i < testCount:
          sync(turn, ds, bump)
          record.n = rand.next
          h = replace(turn, ds, h, record)
          inc i
        else:
          stopActor(turn)

      during(turn, ds, matchRecord("test-observer")):
        bump(turn)
