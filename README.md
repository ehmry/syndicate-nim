# Syndicated Actors for Nim

> The [Syndicated Actor model](https://syndicate-lang.org/about/) is a new model of concurrency, closely related to the actor, tuplespace, and publish/subscribe models.

The Syndicate library provides a code-generator and DSL for writing idiomatic Nim within the excution flow of the Syndicated Actor Model. The library allows for structured conversations with other programs and other languages. It can be used to implement distributed systems but also makes it possible to create dynamically reconfigurable programs using a process controller and a configuration conversation.

## The Preserves Data Language

[Preserves](https://preserves.dev/) is foundational to Syndicate and likewise is the [Nim Preserves](https://git.syndicate-lang.org/ehmry/preserves-nim) library foundation to this Syndicate implementation. When familiarizing yourself with the code here you will need to reference the documentation of that library as well.

## Examples

 ### [test_chat](./tests/test_chat.nim)
Simple chat demo that is compatible with [chat.py](https://git.syndicate-lang.org/syndicate-lang/syndicate-py/src/branch/main/chat.py).
```sh
SYNDICATE_ROUTE='<route [<unix "/run/user/1000/dataspace">] [<ref {oid: "syndicate" sig: #x"69ca300c1dbfa08fba692102dd82311a"}>]>' nim c -r tests/test_chat.nim --user:fnord
```
### [syndicate_utils](https://git.syndicate-lang.org/ehmry/syndicate_utils)

## Drivers

This SAM implementation includes protocol drivers that may be instantiated from a library or as a standalone component.

For drivers that require additional dependencies see [syndicate_utils](https://git.syndicate-lang.org/ehmry/syndicate_utils).

### HTTP
HTTP server and client. HTTPS is not supported.
* [http_driver.prs](./schema/http_driver.prs)
* [http_driver.nim](./src/syndicate/drivers/http_driver.nim)

### Subprocess
Execute UNIX subprocesses and exchange data over stdio using messages.
* [subprocesses.prs](./schema/subprocesses.prs)
* [subprocesses.nim](./src/syndicate/drivers/subprocesses.nim)

## Tracing

Tracing is enabled at compile-time by default, unless disabled with `--define:traceSyndicate=false`.
Tracing is not active at runtime unless the `$SYNDICATE_TRACE_FILE` environmental variable is set.
Tracing is currently not available for solo5.

[![trace](./tests/test_timers.trace.svg)](./tests/test_timers.trace.svg)

There is an experimental tool for converting Syndicate traces to the [Graphviz](https://graphviz.org/) dot format at [trace-to-dot](src/syndicate/private/trace_to_dot.nim).
It should be compatible with [traces](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/trace.prs) from other Syndicate implementations.

## Recommendations

The following are the authors recommendations for using this library and interoperating with external Syndicate components. If you find they differ from Tony Garnock-Jones recommendations, take Tony's recommendations instead.

### Speak the gatekeeper protocol to expose local entities

When providing a service do not expose that service on the initial capability exposed to peers. Require that peers use the [gatekeeper protocol](https://synit.org/book/protocols/syndicate/gatekeeper.html) to resolve service instead. Require the gatekeeper protocol regardless if the peer or transport is "trusted".

Some lines of reasoning leading to this method:

- Components can multiplex services over a single protocol session.
- Clients can "step though" relays directly into a service provided by your component using the standard gatekeeper routing mechanism.
- Failure or success of service negotiation is made explicit.

Furthermore, if a service requires dataspace semantics, provide clients with a dataspace that is local to the service. The reasoning being:

- Local assertions to local dataspaces only create transport traffic during observation by a peer.
- A capability to a local service asserted to a peer can be asserted back to a different service hosted in the same daemon, in which case traffic between the service entities short circuits and becomes local traffic within a daemon (unless the client has amended caveats to a capability).

Example of using the gatekeeper protocol within a Syndicate server configuration:
```
# When the foobar service is required…
? <require-service <service foobar ?detail>>
[
  # Require the foobard daemon…
  <require-service <daemon foobard>>
  ? <service-object <daemon foobard> ?obj>
  [
    # Resolve the service using the gatekeeper protocol and announce the capability
    # using a <service-object <service foobar …> …> assertion.
    let ?rewriter = <* $config [ <rewrite ?resp <service-object <service foobar $detail> $resp>> ]>
    $obj <resolve <$label $detail> $rewriter>
  ]
]
```

Example of a stacked sturdyref route:
```
<route
    [<tcp "example.net" 1024>]
    <ref {oid: examples, sig: #[W1gClLD8dJDFt_NTYqd4xA]}>
    <example-service { turbo-mode-enable: #t }>
    <ref {oid: service-internals, sig: #[8SQ3tSe9r2dNwCnxd-pVvw]}>
  >
```

### Actors and Facets

- Do not share data between actors, use communication instead. Assume that other actors are running on other CPU cores.
- Facets are cheaper than actors. Share data between facets within an actor.

### Observations of Observations

Services can react to clients by observing observations or using `<q …>` and `<a …>` assertions from the [RPC protocol](https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/src/branch/main/schemas/rpc.prs).
Use both.
Observe observations and react to them by locally asserting `<q …>` and responding to the observer using data collected from the corresponding local `<a …>`.
This allows errors in either case to be observed via `<a _ <error ?>>`.

---

This work has been supported by the [NLnet Foundation](https://nlnet.nl/) and the European Commission's [Next Generation Internet programme](https://www.ngi.eu/). The [Syndicate Actor Model](https://syndicate-lang.org/projects/2021/system-layer/) through the [NGI Zero PET](https://nlnet.nl/PET/) program and this library as a part of the [ERIS project](https://eris.codeberg.page/) through [NGI Assure](https://nlnet.nl/assure/).

[![NLnet](./nlnet.svg)](https://nlnet.nl/)
