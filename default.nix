{
  pkgs ? import <nixpkgs> { },
  src ? if pkgs.lib.inNixShell then null else pkgs.lib.cleanSource ./.,
}:
with pkgs;
let
  buildNimSbom = pkgs.callPackage ./build-nim-sbom.nix { };
in
buildNimSbom (finalAttrs: {
  inherit src;
  buildInputs = builtins.attrValues { inherit (pkgs) getdns solo5; };
  nativeBuildInputs = builtins.attrValues { inherit (pkgs) pkg-config solo5; };
  nimFlags = [ "--define:nimPreviewHashRef" ];
}) ./sbom.json
