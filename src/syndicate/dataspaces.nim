# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[hashes, tables],
  pkg/preserves,
  ./actors, ./protocols/dataspace, ./patterns, ./skeletons

from ./protocols/protocol import Handle

export dataspace

type
  Turn = actors.Turn
  Dataspace* {.final.} = ref object of EntityObj
    index*: Index
    handleMap: Table[Handle, Value]

proc dataspacePublish(turn: Turn; e: Entity; v: Value; h: Handle) =
  let ds = Dataspace(e)
  if ds.index.add(turn, v):
    var obs = v.preservesTo(Observe)
    if obs.isSome and obs.get.observer of Cap:
      ds.index.add(turn, obs.get.pattern, Cap(obs.get.observer))
  ds.handleMap[h] = v

proc dataspaceRetract(turn: Turn; e: Entity; h: Handle) =
  let ds = Dataspace(e)
  var v: Value
  if ds.handleMap.pop(h, v) and ds.index.remove(turn, v):
    var obs = v.preservesTo(Observe)
    if obs.isSome and obs.get.observer of Cap:
      ds.index.remove(turn, obs.get.pattern, Cap(obs.get.observer))

proc dataspaceMessage(turn: Turn; e: Entity; v: Value) =
  Dataspace(e).index.deliverMessage(turn, v)

proc newDataspaceEntity*(facet: Facet): Dataspace =
  ## Create a new dataspace owned by the given `Facet`.
  Dataspace(
    facet: facet,
    a: dataspacePublish,
    r: dataspaceRetract,
    m: dataspaceMessage,
    index: initIndex(),
  )

proc newDataspace*(facet: Facet): Cap =
  ## Create a new dataspace owned by the given `Facet`.
  facet.newCap(facet.newDataspaceEntity)

proc newDataspace*(turn: Turn): Cap {.deprecated: "pass a Facet, not a Turn".} =
  turn.facet.newDataspace()

proc observe*(turn: Turn; ds: Cap; pat: Pattern; obs: Cap): Handle {.discardable.} =
  ## Publish an observeration of `pat` by `obs` to the dataspace at `ds`.
  publish(turn, ds, Observe(pattern: pat, observer: obs))
