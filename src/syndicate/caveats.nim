# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## Procedures for generating caveats.

{.push raises: [].}

import
  std/sequtils,
  pkg/preserves,
  ./protocols/sturdy

export sturdy.Caveat, sturdy.Pattern

proc boolean*: Pattern =
  ## Return a caveat `Pattern` that matches boolean.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.Boolean

proc double*: Pattern =
  ## Return a caveat `Pattern` that matches floating-point values.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.Double

proc signedInteger*: Pattern =
  ## Return a caveat `Pattern` that matches integer values.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.SignedInteger

proc string*: Pattern =
  ## Return a caveat `Pattern` that matches string values.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.String

proc byteString*: Pattern =
  ## Return a caveat `Pattern` that matches byte-string values.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.ByteString

proc symbol*: Pattern =
  ## Return a caveat `Pattern` that matches symbol values.
  result = Pattern(orKind: PatternKind.PAtom)
  result.patom = PAtom.Symbol

proc record*(label: Value; fields: varargs[Pattern]): Pattern =
  ## Return a caveat `Pattern` that matches a record class.
  result = Pattern(orKind: PatternKind.PCompound)
  result.pcompound = PCompound(orKind: PCompoundKind.rec)
  result.pcompound.rec.label = label
  result.pcompound.rec.fields = fields.toSeq

proc pnot*(pat: Pattern): Pattern =
  ## Invert a `Pattern`.
  result = Pattern(orKind: PatternKind.PNot)
  result.pnot.pattern = pat

proc reject*(pat: Pattern): Caveat =
  ## Return a `Caveat` that rejects a `Pattern`.
  result = Caveat(orKind: CaveatKind.Reject)
  result.reject.pattern = pat
