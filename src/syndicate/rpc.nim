# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[sets, tables],
  pkg/preserves,
  ./actors, ./patterns, ./protocols/[dataspace, rpc]

export rpc

proc isOk*(res: Result): bool = res.orKind == ResultKind.ok

proc isError*(res: Result): bool = res.orKind == ResultKind.error

proc answerOk*[A, B](turn: Turn; ds: Cap; request: A; response: B) =
  publish(turn, ds, Answer(
      request: request.toPreserves,
      response: ResultOk(value: response.toPreserves).toPreserves,
    ))

proc answerError*[A, B](turn: Turn; ds: Cap; request: A; error: B) =
  publish(turn, ds, Answer(
      request: request.toPreserves,
      response: ResultError(error: error.toPreserves).toPreserves,
    ))

proc getOk*(res: Result): Value =
  if res.orKind == ResultKind.error:
    raise newException(CatchableError, $res.error.error)
  res.ok.value

proc getOk*(res: Result; T: typedesc): T =
  var v = res.getOk
  if not result.fromPreserves(v):
    raise newException(ValueError, "failed to convert result to " & $T & " from " & $v)

type
  QuestionHandler* = proc (turn: Turn; recv: AnswerReceiver; captures: seq[Value]) {.closure.}

  AnswerReceiver* = ref object
    cap: Cap
    req: Value
    h: Handle

proc request*(recv: AnswerReceiver): Value = recv.req

type
  QuestionEntity {.final.} = ref object of EntityObj
    handler: QuestionHandler
    subfacets: Table[Handle, Facet]
    ds: Cap

proc handleQuestion(turn: Turn; e: Entity; v: Value; h: Handle) =
  let e = QuestionEntity(e)
  assert v.isSequence
  assert v.len > 0
  turn.withNewFacet:
    turn.facet.preventInertCheck()
    e.subfacets[h] = turn.facet
    e.handler(turn, AnswerReceiver(
        cap: e.ds, req: v.sequence[0]), v.sequence[1..v.sequence.high])

proc retractQuestion(turn: Turn; e: Entity; h: Handle) =
  let e = QuestionEntity(e)
  var f: Facet
  if e.subfacets.pop(h, f):
    turn.withFacet f: turn.stopFacet()

proc onQuestion*(turn: Turn; ds: Cap; pat: Pattern; handler: QuestionHandler) =
  let cap = QuestionEntity(
      facet: turn.facet, a: handleQuestion, handler: handler, ds: ds).newCap()
  publish(turn, ds, Observe(pattern: Question.match({0: pat.grab()}), observer: cap))

proc respond*(turn: Turn; recv: AnswerReceiver; res: Value) =
  ## Publish an answer with a `Result`.
  replace(turn, recv.cap, recv.h,
      Answer(request: recv.req, response: res))

proc respond*(turn: Turn; recv: AnswerReceiver; res: Result) =
  respond(turn, recv, res.toPreserves)

proc retract*(turn: Turn; recv: AnswerReceiver) =
  retract(turn, recv.h)

proc ok*(turn: Turn; recv: AnswerReceiver; resp: Value) =
  respond(turn, recv, ResultOk(value: resp).toPreserves)

proc ok*[T](turn: Turn; recv: AnswerReceiver; resp: T) =
  ok(turn, recv, resp.toPreserves)

proc error*(turn: Turn; recv: AnswerReceiver; resp: Value) =
  respond(turn, recv, ResultError(error: resp).toPreserves)

template tryAnswer*(turn: Turn; recv: AnswerReceiver; body: untyped): untyped =
  try:
    body
  except CatchableError as err:
    error(turn, recv, err.msg.toPreserves)

type
  ResponseHandler* = proc (turn: Turn; response: Value) {.closure.}

  AnswerEntity {.final.} = ref object of EntityObj
    handler: ResponseHandler
    subfacets: Table[Handle, Facet]

proc receiveResponse(turn: Turn; e: Entity; v: Value; h: Handle) =
  let e = AnswerEntity(e)
  if v.isTuple(1):
    turn.withNewFacet:
      e.subfacets[h] = turn.facet
      e.handler(turn, v[0])

proc retractResult(turn: Turn; e: Entity; h: Handle) =
  let e = AnswerEntity(e)
  var f: Facet
  if e.subfacets.pop(h, f):
    turn.withFacet f: turn.stopFacet()

proc onAnswer*(turn: Turn; ds: Cap; request: Value; handler: ResponseHandler) =
  publish(turn, ds, Question(request: request))
  publish(turn, ds, Observe(
      pattern: Answer.match({0: request.drop, 1: grab()}),
      observer: AnswerEntity(
          facet: turn.facet, a: receiveResponse, handler: handler
        ).newCap(),
    ))

proc onAnswer*[T](turn: Turn; ds: Cap; request: T; handler: ResponseHandler) =
  onAnswer(turn, ds, request.toPreserves, handler)
