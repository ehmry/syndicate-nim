# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/preserves,
  ./actors,
  ./protocols/[gatekeeper, rpc],
  ./rpc

type
  ResolverReceiver {.final.} = ref object of EntityObj
    answer: AnswerReceiver

proc resRecvPublish(turn: Turn; e: Entity; v: Value; h: Handle) =
  let rr = ResolverReceiver(e)
  var resolved: Resolved
  if resolved.fromPreserves(v):
    case resolved.orKind
    of ResolvedKind.accepted:
      ok(turn, rr.answer, resolved.accepted.responderSession)
    of ResolvedKind.Rejected:
      error(turn, rr.answer, resolved.rejected.detail)

proc resRecvRetract(turn: Turn; e: Entity; h: Handle) =
  retract(turn, ResolverReceiver(e).answer)

proc newResolverReceiver*(facet: Facet; answer: AnswerReceiver): Cap =
  facet.newCap(ResolverReceiver(
    facet: facet, a: resRecvPublish, r: resRecvRetract, answer: answer))

type
  Resolver = ref object
    dataspace: Cap
    answer: AnswerReceiver
    route: Route
    transport: ConnectedTransport
    path: ResolvedPath
    transportIndex, pathIndex: int

proc doingOk(turn: Turn; solv: Resolver; res: Result): bool =
  result = res.isOk
  if not result:
    respond(turn, solv.answer, res)

proc walk(turn: Turn; solv: Resolver) =
  assert not solv.path.responderSession.isNil
  let i = solv.pathIndex
  if i >= solv.route.pathSteps.len:
    ok(turn, solv.answer, solv.path)
  else:
    onAnswer(turn, solv.dataspace, ResolvePathStep(
          origin: solv.path.responderSession, pathStep: solv.route.pathSteps[i])
        ) do (
          turn: Turn; resp: Value):
      var res: Result
      if res.fromPreserves(resp):
        if doingOk(turn, solv, res):
          solv.path.responderSession = res.getOk(Cap)
          solv.pathIndex.inc()
          walk(turn, solv)

proc connect(turn: Turn; solv: Resolver) =
  var transport = solv.route.transports[solv.transportIndex]
  onAnswer(turn, solv.dataspace, ConnectTransport(`addr`: transport)) do (
      turn: Turn; resp: Value):
    var res: Result
    if res.fromPreserves resp:
      if res.isError:
        solv.transportIndex.inc()
        if solv.transportIndex < solv.route.transports.len:
          connect(turn, solv)
        else:
          respond(turn, solv.answer, res)
      else:
        solv.transport = res.getOk(ConnectedTransport)
        solv.path.responderSession = solv.transport.responderSession
        solv.path.control = solv.transport.control
        walk(turn, solv)

proc resolve*(turn: Turn; ds: Cap; answer: AnswerReceiver; route: Route) =
  if route.transports.len > 0:
    connect(turn, Resolver(
        dataspace: ds,
        answer: answer,
        route: route,
        ))
