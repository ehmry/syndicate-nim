# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[algorithm, assertions, tables],
  pkg/preserves,
  ./patterns

proc assertSorted(p: Pattern) =
  when not defined(release):
    if p.orKind == PatternKind.group:
      var prev: Value
      for next in p.group.entries.keys:
        doAssert prev.isFalse or (prev < next)
        prev = next

type
  Path* = seq[Value]
  Paths* = seq[Path]
  Captures* = seq[Value]
  Analysis* = tuple
    presentPaths: Paths
    constPaths: Paths
    constValues: seq[Value]
    capturePaths: Paths

proc walk(result: var Analysis; path: var Path; p: Pattern)

proc walk(result: var Analysis; path: var Path; key: Value; pat: Pattern) =
  path.add(key)
  walk(result, path, pat)
  discard path.pop

proc walk(result: var Analysis; path: var Path; p: Pattern) =
  case p.orKind
  of PatternKind.group:
    p.group.entries.sort do (x, y: (Value, Pattern)) -> int: cmp(x, y)
    assertSorted p
    for (k, v) in p.group.entries.pairs:
      walk(result, path, k, v)
  of PatternKind.`bind`:
    result.capturePaths.add(path)
    walk(result, path, p.`bind`.pattern)
  of PatternKind.`discard`:
    result.presentPaths.add(path)
  of PatternKind.`lit`:
    result.constPaths.add(path)
    result.constValues.add(p.`lit`.value.toPreserves)

proc analyse*(p: Pattern): Analysis =
  var path: Path
  walk(result, path, p)

func checkPresence*(v: Value; present: Paths): bool =
  result = true
  for path in present:
    if not result: break
    result = v{path}.isSome

func projectPaths*(v: Value; paths: Paths): Option[Captures] =
  var res = newSeq[Value](paths.len)
  for i, path in paths:
    var vv = v{path}
    if vv.isSome: res[i] = get(vv)
    else: return
  some res

proc capture(pat: Pattern; pr: Value; captures: var seq[Value]): bool =
  ## Recursive capture.
  case pat.orKind
  of PatternKind.`discard`:
    result = true
  of PatternKind.`bind`:
    captures.add pr
    result = capture(pat.`bind`.pattern, pr, captures)
  of PatternKind.lit:
    result = case pat.lit.value.orKind
    of AnyAtomKind.`bool`:
     pr.isBoolean pat.lit.value.bool

    of AnyAtomKind.`double`:
      pr.isFloat pat.lit.value.double

    of AnyAtomKind.`int`:
      pr.isInteger pat.lit.value.int

    of AnyAtomKind.`string`:
      pr.isString pat.lit.value.string

    of AnyAtomKind.`bytes`:
      pr.isByteString pat.lit.value.bytes

    of AnyAtomKind.`symbol`:
      pr.isSymbol pat.lit.value.symbol

    of AnyAtomKind.`embedded`:
      pr.kind == pkEmbedded and
          pat.lit.value.embedded == pr.embeddedRef

  of PatternKind.group:
    result = case pat.group.`type`.orKind
     of rec:
       pr.isRecord and pr.label == pat.group.`type`.rec.label
     of arr:
       pr.isSequence
     of dict:
       pr.isDictionary
    for (key, subPat) in pat.group.entries.pairs:
      if not result: return
      var e = pr{key}
      result =
        if e.isNone: false
        else: capture(subPat, e.get, captures)

proc capture*(pat: Pattern; pr: Value): Option[seq[Value]] =
  ## Capture values from `pr` according to `pat`.
  result = some newSeq[Value]()
  if not capture(pat, pr, result.get):
    reset result

proc matches*(pat: Pattern; pr: Value): bool = capture(pat, pr).isSome
