# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

when traceSyndicate:
  import std/streams
  from std/strutils import toHex

  when defined(solo5):
    proc traceActivation(actor: Actor; act: ActorActivation) =
      echo TraceEntry(
          timestamp: getTime().toUnixFloat(),
          actor: actor.id.toPreserves,
           item: act).toPreserves

  else:
    from std/os import getEnv

    proc openTraceStream: FileStream =
      let path = getEnv("SYNDICATE_TRACE_FILE")
      case path
      of "": discard
      of "-": result = newFileStream(stderr)
      else: result = openFileStream(path, fmWrite)

    let traceStream = openTraceStream()

    proc traceActivation(actor: Actor; act: ActorActivation) =
      if not traceStream.isNil:
        var entry = TraceEntry(
            timestamp: getTime().toUnixFloat(),
            actor: actor.id.toPreserves,
            item: act)
        traceStream.write(entry.toPreserves)
        traceStream.flush()

  proc allocTurnId(): TurnId =
    result = nextTurnId.toPreserves
    inc(nextTurnId, 10)

  proc path(facet: Facet): seq[Value] =
    var f = facet
    while not f.isNil:
      result.add f.id.toPreserves
      f = f.parent

  proc dequeue(turn: TurnRef; event: TargetedTurnEvent) =
    var act: ActionDescription
    if event.target.actor == turn.actor.id.toPreserves:
      act = ActionDescription(orKind: dequeueInternal)
      act.dequeueinternal.event = event
    else:
      act = ActionDescription(orKind: dequeue)
      act.dequeue.event = event
    turn.desc.actions.add act

  proc toTraceTarget(cap: Cap): Target =
    assert not cap.target.isNil
    assert not cap.target.facet.isNil
    result.actor = cap.target.actor.id.toPreserves
    result.facet = cap.target.facet.id.toPreserves
    result.oid = cap.target.oid.toPreserves

  proc labels(f: Facet): string =
    assert not f.isNil
    assert not f.actor.isNil
    result.add f.actor.name
    proc catLabels(f: Facet; labels: var string) =
      if not f.parent.isNil:
        catLabels(f.parent, labels)
        labels.add ':'
        labels.add $f.id
    catLabels(f, result)

  proc `$`*(f: Facet): string =
    "<Facet:" & f.labels & ">"

  proc `$`*(actor: Actor): string =
    "<Actor:" & actor.name & ">" # TODO: ambigous

  proc `$`*(cap: Cap): string =
    result.add $cap.relay.actor.id
    result.add"/"
    result.add $cap.relay.id
    result.add ":"
    result.add cast[uint](cap[].addr).toHex

  proc `$`*(t: Turn): string =
    "<Turn:" & $t.desc.id & ">"

  proc decap(v: Value): Value =
    mapEmbeds(v) do (e: Value) -> Value:
      result = ($e.embeddedRef.Cap).toSymbol
      result.embedded = true

proc initEvent(turn: Turn; cap: Cap): TargetedTurnEvent =
  when traceSyndicate:
    result = TargetedTurnEvent(target: Target(
        actor: turn.actor.id.toPreserves,
        facet: turn.facet.id.toPreserves,
        oid: cap.target.oid.toPreserves))

proc externalCause*[T](description: T): TurnCause =
  ## Create a `TurnCause` from an arbitrary value.
  ## This is a no-op when tracing is disabled.
  when traceSyndicate:
    result = TurnCause(orKind: TurnCauseKind.external)
    result.external.description = description.toPreserves

proc causedBy(prev: Turn): TurnCause =
  when traceSyndicate:
    result = TurnCause(orKind: TurnCauseKind.turn)
    result.turn.id = prev.desc.id
