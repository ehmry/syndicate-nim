# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

proc newTurn(facet: Facet; cause: TurnCause): TurnRef =
  result = TurnRef(facet: facet)
  when traceSyndicate:
    result.desc.id = allocTurnId()
    result.desc.cause = cause

template withFacet*(turn: Turn; inner: Facet; body: untyped): untyped =
  ## Execute `body` with `turn` scoped to the `inner` `Facet`.
  ## The original `Facet` is restored on return.
  let outer = turn.facet
  assert outer.actor == inner.actor
  turn.facet = inner
  block:
    body
  turn.facet = outer

template withNewFacet*(turn: Turn; body: untyped): untyped =
  ## Execute `body` with `turn` scoped to a new `Facet`.
  ## The original `Facet` is restored on return.
  turn.withFacet newFacet(turn, turn.facet):
    body
    inertCheck(turn)

proc queueTurn*(facet: Facet; cause: TurnCause; act: TurnAction) =
  assert not facet.isNil
  var turn = facet.newTurn(cause)
  turn.work.addLast((facet, act,))
  turnQueue.addLast(turn)

proc queueWork(turn: Turn; facet: Facet; act: TurnAction) =
  assert turn.state != turnOver
  assert not facet.isNil
  if turn.facet == facet:
    turn.work.addLast((facet, act,))
  else:
    queueTurn(facet, causedBy turn, act)

proc queueEffect(turn: Turn; entity: Entity; event: TargetedTurnEvent; affect: TurnAction) =
  ## Queue an effect to `entity` to the end of this `Turn`.
  ## If the `Entity` is local to this actor then it will
  ## queue later in this `Turn`.
  when traceSyndicate:
    var act: ActionDescription
    if entity.actor == turn.actor:
      act = ActionDescription(orKind: enqueueinternal)
      act.enqueueinternal.event = event
    else:
      act = ActionDescription(orKind: enqueue)
      act.enqueue.event = event
    turn.desc.actions.add act
  if entity.actor == turn.actor:
    turn.work.addLast((entity.facet, affect,))
  else:
    var otherTurn = turn.effects.getOrDefault(entity)
    if otherTurn.isNil:
      otherTurn = entity.facet.newTurn(causedBy turn)
      turn.effects[entity] = otherTurn
    otherTurn.queueWork(entity.facet, affect)

proc fail*(actor: Actor; cause: string) =
  queueTurn(actor.root, externalCause cause) do (turn: Turn):
    stopActor(turn, cause, Value())

proc fail*(actor: Actor; cause: string; err: ref Exception) =
  actor.exitReason = err
  queueTurn(actor.root, externalCause cause) do (turn: Turn):
    stopActor(turn, $err.name, err.msg.toPreserves)
      # TODO: dump the full exception into the trace log.

proc run(turn: Turn) =
  while turn.work.len > 0:
    let (f, act) = turn.work.popFirst()
    turn.withFacet f: act(turn)
  when traceSyndicate:
    var act = ActorActivation(orKind: ActorActivationKind.turn)
    act.turn = move turn.desc
    traceActivation(turn.actor, act)
  # TODO: catch exceptions here
  for eff in turn.effects.mvalues:
    assert not eff.facet.isNil
    turnQueue.addLast eff
  turn.state = turnOver
