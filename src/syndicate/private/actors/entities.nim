# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

proc publish(turn: Turn; cap: Cap; ass: Value; h: Handle) =
  assert cap.isActive, $cap.relay.state
  if cap.isActive:
    let ass = ass.attenuate(cap.caveats)
    if not ass.isFalse:
      let e = OutboundAssertion(handle: h, peer: cap)
      turn.facet.outbound[h] = e
      var event = initEvent(turn, cap)
      when traceSyndicate:
        event.detail = trace.TurnEvent(orKind: trace.TurnEventKind.assert)
        event.detail.assert.assertion.value.value = decap ass
        event.detail.assert.handle = h
      turn.queueEffect(cap.target, event) do (turn: Turn):
        e.established = true
        when traceSyndicate: turn.dequeue(event)
        if not cap.target.a.isNil:
          cap.target.a(turn, cap.target, ass, e.handle)

proc publish*(turn: Turn; cap: Cap; ass: Value): Handle {.discardable.} =
  result = allocHandle()
  publish(turn, cap, ass, result)

proc publish*[T](turn: Turn; cap: Cap; ass: T): Handle {.discardable.} =
  publish(turn, cap, ass.toPreserves)

proc retract(turn: Turn; e: OutboundAssertion) =
  var event = initEvent(turn, e.peer)
  when traceSyndicate:
    event.detail = trace.TurnEvent(orKind: TurnEventKind.retract)
    event.detail.retract.handle = e.handle
  turn.queueEffect(e.peer.target, event) do (turn: Turn):
    when traceSyndicate: turn.dequeue(event)
    if e.established:
      e.established = false
      if not e.peer.target.r.isNil:
        e.peer.target.r(turn, e.peer.target, e.handle)

proc retract*(turn: Turn; h: Handle) =
  var e: OutboundAssertion
  if turn.facet.outbound.pop(h, e):
    turn.retract(e)

proc replace*[T](turn: Turn; cap: Cap; h: Handle; ass: T): Handle =
  result = publish(turn, cap, ass)
  if h != default(Handle):
    retract(turn, h)

proc replace*[T](turn: Turn; cap: Cap; h: var Handle; ass: T): Handle {.discardable.} =
  var old = h
  h = publish(turn, cap, ass)
  if old != default(Handle):
    retract(turn, old)
  h

proc message*(turn: Turn; cap: Cap; msg: Value) =
  assert cap.isActive, $cap.relay.state
  if cap.isActive:
    let msg = msg.attenuate(cap.caveats)
    if not msg.isFalse:
      var event = initEvent(turn, cap)
      when traceSyndicate:
        event.detail = trace.TurnEvent(orKind: TurnEventKind.message)
        event.detail.message.body.value.value = decap msg
      turn.queueEffect(cap.target, event) do (turn: Turn):
        when traceSyndicate: turn.dequeue(event)
        if not cap.target.m.isNil:
          cap.target.m(turn, cap.target, msg)

proc message*[T](turn: Turn; r: Cap; v: T) =
  message(turn, r, v.toPreserves)

proc sync*(turn: Turn; dest, peer: Cap) =
  assert dest.isActive, $dest.relay.state
  if dest.isActive and peer.isActive:
    var event = initEvent(turn, dest)
    when traceSyndicate:
      event.detail = trace.TurnEvent(orKind: TurnEventKind.sync)
      event.detail.sync.peer = peer.toTraceTarget
    turn.queueEffect(dest.target, event) do (turn: Turn):
      when traceSyndicate: turn.dequeue(event)
      if not dest.target.s.isNil:
        dest.target.s(turn, dest.target, peer)
      else:
        queueTurn(dest.target.facet, causedBy turn) do (turn: Turn):
          message(turn, peer, true.toPreserves)
            # complete the sync on a later turn

type
  SyncResponseHandler {.final.} = ref object of EntityObj
    work: TurnAction

proc handleSyncResponse(turn: Turn; e: Entity; msg: Value) {.nimcall.} =
  if msg.isBoolean true:
    let e = SyncResponseHandler(e)
    e.facet.inertCheckPreventers.dec()
    e.work(turn)

proc sync*(turn: Turn; refer: Cap; act: TurnAction) =
  let e = SyncResponseHandler(
      facet: turn.facet, m: handleSyncResponse, work: act)
  e.facet.inertCheckPreventers.inc()
  sync(turn, refer, e.newCap)

type
  ClosureEntity {.final.} = ref object of EntityObj
    # An Entity with closures rather than nimcalls.
    publishClosure: PublishClosure
    retractClosure: RetractClosure
    messageClosure: MessageClosure
    syncClosure: SyncClosure

  PublishClosure* = proc (turn: Turn; v: Value; h: Handle) {.closure.}
  RetractClosure* = proc (turn: Turn; h: Handle) {.closure.}
  MessageClosure* = proc (turn: Turn; v: Value) {.closure.}
  SyncClosure* = proc (turn: Turn; peer: Cap) {.closure.}

proc closurePublish(turn: Turn; e: Entity; v: Value; h: Handle) {.nimcall.} =
  let ce = ClosureEntity(e)
  if ce.publishClosure != nil: ce.publishClosure(turn, v, h)

proc closureRetract(turn: Turn; e: Entity; h: Handle) {.nimcall.} =
  let ce = ClosureEntity(e)
  if ce.retractClosure != nil: ce.retractClosure(turn, h)

proc closureMessage(turn: Turn; e: Entity; v: Value) {.nimcall.} =
  let ce = ClosureEntity(e)
  if ce.messageClosure != nil: ce.messageClosure(turn, v)

proc closureSync(turn: Turn; e: Entity; peer: Cap) {.nimcall.} =
  let ce = ClosureEntity(e)
  if ce.syncClosure != nil: ce.syncClosure(turn, peer)

proc newEntity*(
      facet: Facet;
      publish: PublishClosure = nil;
      retract: RetractClosure = nil;
      message: MessageClosure = nil;
      sync: SyncClosure = nil
    ): Entity =
  ## Create a new `Entity` implemented using closure procs.
  let ce = ClosureEntity(
      facet: facet,
      publishClosure: publish,
      retractClosure: retract,
      messageClosure: message,
      syncClosure: sync,
    )
  if not ce.publishClosure.isNil:
    ce.a = closurePublish
  if not ce.retractClosure.isNil:
    ce.r = closureRetract
  if not ce.retractClosure.isNil:
    ce.m = closureMessage
  if not ce.retractClosure.isNil:
    ce.s = closureSync
  ce

proc newPublishHandler*(facet: Facet; clos: PublishClosure): Cap =
  facet.newCap(ClosureEntity(facet: facet, a: closurePublish, publishClosure: clos))

proc newRetractHandler*(facet: Facet; clos: RetractClosure): Cap =
  facet.newCap(ClosureEntity(facet: facet, r: closureRetract, retractClosure: clos))

proc newMessageHandler*(facet: Facet; clos: MessageClosure): Cap =
  facet.newCap(ClosureEntity(facet: facet, m: closureMessage, messageClosure: clos))

proc newSyncHandler*(facet: Facet; clos: SyncClosure): Cap =
  facet.newCap(ClosureEntity(facet: facet, s: closureSync, syncClosure: clos))
