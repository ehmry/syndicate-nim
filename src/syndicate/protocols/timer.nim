
import
  preserves

type
  LaterThan* {.preservesRecord: "later-than".} = object
    `seconds`*: float
  `TimerKind`* {.preservesOr, pure.} = enum
    `relative`, `absolute`, `clear`
  SetTimer* {.preservesRecord: "set-timer".} = object
    `label`*: Value
    `seconds`*: float
    `kind`*: TimerKind
    `peer`* {.preservesEmbedded.}: Value
  TimerExpired* {.preservesRecord: "timer-expired".} = object
    `label`*: Value
    `seconds`*: float
proc `$`*(x: LaterThan | SetTimer | TimerExpired): string =
  `$`(toPreserves(x))

proc encode*(x: LaterThan | SetTimer | TimerExpired): seq[byte] =
  encode(toPreserves(x))
