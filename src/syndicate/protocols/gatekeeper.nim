
import
  preserves

type
  BindObserverKind* {.pure.} = enum
    `absent`, `present`
  `BindObserver`* {.preservesOr.} = object
    case orKind*: BindObserverKind
    of BindObserverKind.`absent`:
      `absent`* {.preservesLiteral: "#f".}: bool
    of BindObserverKind.`present`:
      `present`*: EmbeddedRef
  TransportControl* = ForceDisconnect
  Description* = Value
  ResolvedPathStep* = EmbeddedRef
  ResolvePath* {.preservesRecord: "resolve-path".} = object
    `route`*: Route
  PathStep* = Value
  ConnectTransport* {.preservesRecord: "connect-transport".} = object
    `addr`*: Value
  ResolvedKind* {.pure.} = enum
    `Rejected`, `accepted`
  ResolvedAccepted* {.preservesRecord: "accepted".} = object
    `responderSession`* {.preservesEmbedded.}: EmbeddedRef
  `Resolved`* {.preservesOr.} = object
    case orKind*: ResolvedKind
    of ResolvedKind.`Rejected`:
      `rejected`*: Rejected
    of ResolvedKind.`accepted`:
      `accepted`*: ResolvedAccepted
  ResolvePathStep* {.preservesRecord: "resolve-path-step".} = object
    `origin`* {.preservesEmbedded.}: EmbeddedRef
    `pathStep`*: PathStep
  Step* = Value
  Bind* {.preservesRecord: "bind".} = object
    `description`*: Description
    `target`* {.preservesEmbedded.}: EmbeddedRef
    `observer`*: BindObserver
  ConnectedTransport* {.preservesRecord: "connected-transport".} = object
    `addr`*: Value
    `control`* {.preservesEmbedded.}: EmbeddedRef
    `responderSession`* {.preservesEmbedded.}: EmbeddedRef
  BoundKind* {.pure.} = enum
    `Rejected`, `bound`
  BoundBound* {.preservesRecord: "bound".} = object
    `pathStep`*: PathStep
  `Bound`* {.preservesOr.} = object
    case orKind*: BoundKind
    of BoundKind.`Rejected`:
      `rejected`*: Rejected
    of BoundKind.`bound`:
      `bound`*: BoundBound
  ForceDisconnect* {.preservesRecord: "force-disconnect".} = object
  ResolvedPath* {.preservesRecord: "resolved-path".} = object
    `addr`*: Value
    `control`* {.preservesEmbedded.}: EmbeddedRef
    `responderSession`* {.preservesEmbedded.}: EmbeddedRef
  Rejected* {.preservesRecord: "rejected".} = object
    `detail`*: Value
  Resolve* {.preservesRecord: "resolve".} = object
    `step`*: Step
    `observer`* {.preservesEmbedded.}: EmbeddedRef
  Route* {.preservesRecord: "route".} = object
    `transports`*: seq[Value]
    `pathSteps`* {.preservesTupleTail.}: seq[PathStep]
proc `$`*(x: BindObserver | TransportControl | ResolvedPathStep | ResolvePath |
    ConnectTransport |
    Resolved |
    ResolvePathStep |
    Bind |
    ConnectedTransport |
    Bound |
    ForceDisconnect |
    ResolvedPath |
    Rejected |
    Resolve |
    Route): string =
  `$`(toPreserves(x))

proc encode*(x: BindObserver | TransportControl | ResolvedPathStep | ResolvePath |
    ConnectTransport |
    Resolved |
    ResolvePathStep |
    Bind |
    ConnectedTransport |
    Bound |
    ForceDisconnect |
    ResolvedPath |
    Rejected |
    Resolve |
    Route): seq[byte] =
  encode(toPreserves(x))
