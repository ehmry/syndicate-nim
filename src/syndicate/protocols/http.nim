
import
  preserves, std/tables

type
  MimeType* = Symbol
  HostPatternKind* {.pure.} = enum
    `any`, `host`
  `HostPattern`* {.preservesOr.} = object
    case orKind*: HostPatternKind
    of HostPatternKind.`any`:
      `any`* {.preservesLiteral: "#f".}: bool
    of HostPatternKind.`host`:
      `host`*: string
  HttpService* {.preservesRecord: "http-service".} = object
    `host`*: HostPattern
    `port`*: BiggestInt
    `method`*: MethodPattern
    `path`*: PathPattern
  HttpBinding* {.preservesRecord: "http-bind".} = object
    `host`*: HostPattern
    `port`*: BiggestInt
    `method`*: MethodPattern
    `path`*: PathPattern
    `handler`* {.preservesEmbedded.}: Value
  HttpResponseKind* {.pure.} = enum
    `status`, `header`, `chunk`, `done`
  HttpResponseStatus* {.preservesRecord: "status".} = object
    `code`*: BiggestInt
    `message`*: string
  HttpResponseHeader* {.preservesRecord: "header".} = object
    `name`*: Symbol
    `value`*: string
  HttpResponseChunk* {.preservesRecord: "chunk".} = object
    `chunk`*: Chunk
  HttpResponseDone* {.preservesRecord: "done".} = object
    `chunk`*: Chunk
  `HttpResponse`* {.preservesOr.} = object
    case orKind*: HttpResponseKind
    of HttpResponseKind.`status`:
      `status`*: HttpResponseStatus
    of HttpResponseKind.`header`:
      `header`*: HttpResponseHeader
    of HttpResponseKind.`chunk`:
      `chunk`*: HttpResponseChunk
    of HttpResponseKind.`done`:
      `done`*: HttpResponseDone
  QueryValueKind* {.pure.} = enum
    `string`, `file`
  QueryValueFile* {.preservesRecord: "file".} = object
    `filename`*: string
    `headers`*: Headers
    `body`*: seq[byte]
  `QueryValue`* {.preservesOr.} = object
    case orKind*: QueryValueKind
    of QueryValueKind.`string`:
      `string`*: string
    of QueryValueKind.`file`:
      `file`*: QueryValueFile
  MethodPatternKind* {.pure.} = enum
    `any`, `specific`
  `MethodPattern`* {.preservesOr.} = object
    case orKind*: MethodPatternKind
    of MethodPatternKind.`any`:
      `any`* {.preservesLiteral: "#f".}: bool
    of MethodPatternKind.`specific`:
      `specific`*: Symbol
  PathPatternElementKind* {.pure.} = enum
    `label`, `wildcard`, `rest`
  `PathPatternElement`* {.preservesOr.} = object
    case orKind*: PathPatternElementKind
    of PathPatternElementKind.`label`:
      `label`*: string
    of PathPatternElementKind.`wildcard`:
      `wildcard`* {.preservesLiteral: "_".}: bool
    of PathPatternElementKind.`rest`:
      `rest`* {.preservesLiteral: "...".}: bool
  PathPattern* = seq[PathPatternElement]
  HttpListener* {.preservesRecord: "http-listener".} = object
    `port`*: BiggestInt
  HttpRequest* {.preservesRecord: "http-request".} = object
    `sequenceNumber`*: BiggestInt
    `host`*: RequestHost
    `port`*: BiggestInt
    `method`*: Symbol
    `path`*: seq[string]
    `headers`*: Headers
    `query`*: Table[Symbol, seq[QueryValue]]
    `body`*: Value
  HttpContext* {.preservesRecord: "request".} = object
    `req`*: HttpRequest
    `res`* {.preservesEmbedded.}: Value
  RequestHostKind* {.pure.} = enum
    `absent`, `present`
  `RequestHost`* {.preservesOr.} = object
    case orKind*: RequestHostKind
    of RequestHostKind.`absent`:
      `absent`* {.preservesLiteral: "#f".}: bool
    of RequestHostKind.`present`:
      `present`*: string
  ChunkKind* {.pure.} = enum
    `string`, `bytes`
  `Chunk`* {.preservesOr.} = object
    case orKind*: ChunkKind
    of ChunkKind.`string`:
      `string`*: string
    of ChunkKind.`bytes`:
      `bytes`*: seq[byte]
  Headers* = Table[Symbol, string]
proc `$`*(x: MimeType | HostPattern | HttpService | HttpBinding | HttpResponse |
    QueryValue |
    MethodPattern |
    PathPatternElement |
    PathPattern |
    HttpListener |
    HttpRequest |
    HttpContext |
    RequestHost |
    Chunk |
    Headers): string =
  `$`(toPreserves(x))

proc encode*(x: MimeType | HostPattern | HttpService | HttpBinding |
    HttpResponse |
    QueryValue |
    MethodPattern |
    PathPatternElement |
    PathPattern |
    HttpListener |
    HttpRequest |
    HttpContext |
    RequestHost |
    Chunk |
    Headers): seq[byte] =
  encode(toPreserves(x))
