
import
  preserves, std/options, std/tables

type
  EnvVariableKind* {.pure.} = enum
    `string`, `symbol`, `invalid`
  `EnvVariable`* {.preservesOr.} = object
    case orKind*: EnvVariableKind
    of EnvVariableKind.`string`:
      `string`*: string
    of EnvVariableKind.`symbol`:
      `symbol`*: Symbol
    of EnvVariableKind.`invalid`:
      `invalid`*: Value
  EOF* {.preservesRecord: "eof".} = object
  Signal* {.preservesRecord: "signal".} = object
    `num`*: BiggestInt
  StdinProtocolKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  StdinProtocolPresent* {.preservesDictionary.} = object
    `stdin-protocol`*: Symbol
  StdinProtocolInvalid* {.preservesDictionary.} = object
    `stdin-protocol`*: Value
  StdinProtocolAbsent* {.preservesDictionary.} = object
  `StdinProtocol`* {.preservesOr.} = object
    case orKind*: StdinProtocolKind
    of StdinProtocolKind.`present`:
      `present`*: StdinProtocolPresent
    of StdinProtocolKind.`invalid`:
      `invalid`*: StdinProtocolInvalid
    of StdinProtocolKind.`absent`:
      `absent`*: StdinProtocolAbsent
  ControlMessageFromSubprocessKind* {.pure.} = enum
    `EOF`, `Exit`
  `ControlMessageFromSubprocess`* {.preservesOr.} = object
    case orKind*: ControlMessageFromSubprocessKind
    of ControlMessageFromSubprocessKind.`EOF`:
      `eof`*: EOF
    of ControlMessageFromSubprocessKind.`Exit`:
      `exit`*: Exit
  SubprocessAdapterDetailClearEnv* = Option[Value]
  SubprocessAdapterDetailDir* = Option[Value]
  SubprocessAdapterDetailEnv* = Option[Value]
  SubprocessAdapterDetailStderr* = Option[Value]
  SubprocessAdapterDetailStdinProtocol* = Option[Value]
  SubprocessAdapterDetailStdout* = EmbeddedRef
  SubprocessAdapterDetailStdoutProtocol* = Option[Value]
  `SubprocessAdapterDetail`* {.preservesDictionary.} = object
    `argv`*: CommandLine
    `clearEnv`*: Option[Value]
    `dir`*: Option[Value]
    `env`*: Option[Value]
    `stderr`*: Option[Value]
    `stdin-protocol`*: Option[Value]
    `stdout`* {.preservesEmbedded.}: EmbeddedRef
    `stdout-protocol`*: Option[Value]
  ProcessEnvKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  ProcessEnvPresent* {.preservesDictionary.} = object
    `env`*: Table[EnvVariable, EnvValue]
  ProcessEnvInvalid* {.preservesDictionary.} = object
    `env`*: Value
  ProcessEnvAbsent* {.preservesDictionary.} = object
  `ProcessEnv`* {.preservesOr.} = object
    case orKind*: ProcessEnvKind
    of ProcessEnvKind.`present`:
      `present`*: ProcessEnvPresent
    of ProcessEnvKind.`invalid`:
      `invalid`*: ProcessEnvInvalid
    of ProcessEnvKind.`absent`:
      `absent`*: ProcessEnvAbsent
  ProcessDirKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  ProcessDirPresent* {.preservesDictionary.} = object
    `dir`*: string
  ProcessDirInvalid* {.preservesDictionary.} = object
    `dir`*: Value
  ProcessDirAbsent* {.preservesDictionary.} = object
  `ProcessDir`* {.preservesOr.} = object
    case orKind*: ProcessDirKind
    of ProcessDirKind.`present`:
      `present`*: ProcessDirPresent
    of ProcessDirKind.`invalid`:
      `invalid`*: ProcessDirInvalid
    of ProcessDirKind.`absent`:
      `absent`*: ProcessDirAbsent
  ClearEnvKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  ClearEnvPresent* {.preservesDictionary.} = object
    `clearEnv`*: bool
  ClearEnvInvalid* {.preservesDictionary.} = object
    `clearEnv`*: Value
  ClearEnvAbsent* {.preservesDictionary.} = object
  `ClearEnv`* {.preservesOr.} = object
    case orKind*: ClearEnvKind
    of ClearEnvKind.`present`:
      `present`*: ClearEnvPresent
    of ClearEnvKind.`invalid`:
      `invalid`*: ClearEnvInvalid
    of ClearEnvKind.`absent`:
      `absent`*: ClearEnvAbsent
  SubprocessAdapterStep* {.preservesRecord: "unix-subprocess".} = object
    `detail`*: SubprocessAdapterDetail
  StderrCapKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  StderrCapPresent* {.preservesDictionary.} = object
    `stderr`* {.preservesEmbedded.}: EmbeddedRef
  StderrCapInvalid* {.preservesDictionary.} = object
    `stderr`*: Value
  StderrCapAbsent* {.preservesDictionary.} = object
  `StderrCap`* {.preservesOr.} = object
    case orKind*: StderrCapKind
    of StderrCapKind.`present`:
      `present`*: StderrCapPresent
    of StderrCapKind.`invalid`:
      `invalid`*: StderrCapInvalid
    of StderrCapKind.`absent`:
      `absent`*: StderrCapAbsent
  CommandLineKind* {.pure.} = enum
    `shell`, `full`
  `CommandLine`* {.preservesOr.} = object
    case orKind*: CommandLineKind
    of CommandLineKind.`shell`:
      `shell`*: string
    of CommandLineKind.`full`:
      `full`*: FullCommandLine
  StdoutProtocolKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  StdoutProtocolPresent* {.preservesDictionary.} = object
    `stdout-protocol`*: Symbol
  StdoutProtocolInvalid* {.preservesDictionary.} = object
    `stdout-protocol`*: Value
  StdoutProtocolAbsent* {.preservesDictionary.} = object
  `StdoutProtocol`* {.preservesOr.} = object
    case orKind*: StdoutProtocolKind
    of StdoutProtocolKind.`present`:
      `present`*: StdoutProtocolPresent
    of StdoutProtocolKind.`invalid`:
      `invalid`*: StdoutProtocolInvalid
    of StdoutProtocolKind.`absent`:
      `absent`*: StdoutProtocolAbsent
  FullCommandLine* {.preservesTuple.} = object
    `program`*: string
    `args`* {.preservesTupleTail.}: seq[string]
  EnvValueKind* {.pure.} = enum
    `set`, `remove`, `invalid`
  `EnvValue`* {.preservesOr.} = object
    case orKind*: EnvValueKind
    of EnvValueKind.`set`:
      `set`*: string
    of EnvValueKind.`remove`:
      `remove`* {.preservesLiteral: "#f".}: bool
    of EnvValueKind.`invalid`:
      `invalid`*: Value
  Exit* {.preservesRecord: "exit".} = object
    `detail`*: Value
  ControlMessageForSubprocessKind* {.pure.} = enum
    `EOF`, `Signal`
  `ControlMessageForSubprocess`* {.preservesOr.} = object
    case orKind*: ControlMessageForSubprocessKind
    of ControlMessageForSubprocessKind.`EOF`:
      `eof`*: EOF
    of ControlMessageForSubprocessKind.`Signal`:
      `signal`*: Signal
proc `$`*(x: EnvVariable | EOF | Signal | StdinProtocol |
    ControlMessageFromSubprocess |
    SubprocessAdapterDetail |
    ProcessEnv |
    ProcessDir |
    ClearEnv |
    SubprocessAdapterStep |
    StderrCap |
    CommandLine |
    StdoutProtocol |
    FullCommandLine |
    EnvValue |
    Exit |
    ControlMessageForSubprocess): string =
  `$`(toPreserves(x))

proc encode*(x: EnvVariable | EOF | Signal | StdinProtocol |
    ControlMessageFromSubprocess |
    SubprocessAdapterDetail |
    ProcessEnv |
    ProcessDir |
    ClearEnv |
    SubprocessAdapterStep |
    StderrCap |
    CommandLine |
    StdoutProtocol |
    FullCommandLine |
    EnvValue |
    Exit |
    ControlMessageForSubprocess): seq[byte] =
  encode(toPreserves(x))
