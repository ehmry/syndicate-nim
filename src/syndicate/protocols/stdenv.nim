
import
  preserves, gatekeeper, sturdy

type
  StandardTransportKind* {.pure.} = enum
    `wsUrl`, `other`
  `StandardTransport`* {.preservesOr.} = object
    case orKind*: StandardTransportKind
    of StandardTransportKind.`wsUrl`:
      `wsurl`*: string
    of StandardTransportKind.`other`:
      `other`*: Value
  StandardRouteKind* {.pure.} = enum
    `standard`, `general`
  StandardRouteStandard* {.preservesTuple.} = object
    `transports`*: seq[StandardTransport]
    `key`*: seq[byte]
    `service`*: Value
    `sig`*: seq[byte]
    `oid`*: Value
    `caveats`* {.preservesTupleTail.}: seq[sturdy.Caveat]
  `StandardRoute`* {.preservesOr.} = object
    case orKind*: StandardRouteKind
    of StandardRouteKind.`standard`:
      `standard`*: StandardRouteStandard
    of StandardRouteKind.`general`:
      `general`*: gatekeeper.Route
proc `$`*(x: StandardTransport | StandardRoute): string =
  `$`(toPreserves(x))

proc encode*(x: StandardTransport | StandardRoute): seq[byte] =
  encode(toPreserves(x))
