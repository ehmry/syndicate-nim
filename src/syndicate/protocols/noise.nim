
import
  preserves, std/options

type
  SessionItemKind* {.pure.} = enum
    `Initiator`, `Packet`
  `SessionItem`* {.preservesOr.} = object
    case orKind*: SessionItemKind
    of SessionItemKind.`Initiator`:
      `initiator`*: Initiator
    of SessionItemKind.`Packet`:
      `packet`*: Packet
  NoisePreSharedKeysKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  NoisePreSharedKeysPresent* {.preservesDictionary.} = object
    `preSharedKeys`*: seq[seq[byte]]
  NoisePreSharedKeysInvalid* {.preservesDictionary.} = object
    `preSharedKeys`*: Value
  NoisePreSharedKeysAbsent* {.preservesDictionary.} = object
  `NoisePreSharedKeys`* {.preservesOr.} = object
    case orKind*: NoisePreSharedKeysKind
    of NoisePreSharedKeysKind.`present`:
      `present`*: NoisePreSharedKeysPresent
    of NoisePreSharedKeysKind.`invalid`:
      `invalid`*: NoisePreSharedKeysInvalid
    of NoisePreSharedKeysKind.`absent`:
      `absent`*: NoisePreSharedKeysAbsent
  Initiator* {.preservesRecord: "initiator".} = object
    `initiatorSession`* {.preservesEmbedded.}: EmbeddedRef
  PacketKind* {.pure.} = enum
    `complete`, `fragmented`
  `Packet`* {.preservesOr.} = object
    case orKind*: PacketKind
    of PacketKind.`complete`:
      `complete`*: seq[byte]
    of PacketKind.`fragmented`:
      `fragmented`*: seq[seq[byte]]
  NoiseProtocolKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  NoiseProtocolPresent* {.preservesDictionary.} = object
    `protocol`*: string
  NoiseProtocolInvalid* {.preservesDictionary.} = object
    `protocol`*: Value
  NoiseProtocolAbsent* {.preservesDictionary.} = object
  `NoiseProtocol`* {.preservesOr.} = object
    case orKind*: NoiseProtocolKind
    of NoiseProtocolKind.`present`:
      `present`*: NoiseProtocolPresent
    of NoiseProtocolKind.`invalid`:
      `invalid`*: NoiseProtocolInvalid
    of NoiseProtocolKind.`absent`:
      `absent`*: NoiseProtocolAbsent
  NoiseStepDetail* = ServiceSelector
  SecretKeyFieldKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  SecretKeyFieldPresent* {.preservesDictionary.} = object
    `secretKey`*: seq[byte]
  SecretKeyFieldInvalid* {.preservesDictionary.} = object
    `secretKey`*: Value
  SecretKeyFieldAbsent* {.preservesDictionary.} = object
  `SecretKeyField`* {.preservesOr.} = object
    case orKind*: SecretKeyFieldKind
    of SecretKeyFieldKind.`present`:
      `present`*: SecretKeyFieldPresent
    of SecretKeyFieldKind.`invalid`:
      `invalid`*: SecretKeyFieldInvalid
    of SecretKeyFieldKind.`absent`:
      `absent`*: SecretKeyFieldAbsent
  NoisePathStepDetail* = NoiseSpec
  NoiseServiceSpecKey* = seq[byte]
  NoiseServiceSpecPreSharedKeys* = Option[Value]
  NoiseServiceSpecProtocol* = Option[Value]
  NoiseServiceSpecSecretKey* = Option[Value]
  `NoiseServiceSpec`* {.preservesDictionary.} = object
    `key`*: seq[byte]
    `preSharedKeys`*: Option[Value]
    `protocol`*: Option[Value]
    `secretKey`*: Option[Value]
    `service`*: ServiceSelector
  NoiseDescriptionDetail* = NoiseServiceSpec
  ServiceSelector* = Value
  NoiseSpecKey* = seq[byte]
  NoiseSpecPreSharedKeys* = Option[Value]
  NoiseSpecProtocol* = Option[Value]
  `NoiseSpec`* {.preservesDictionary.} = object
    `key`*: seq[byte]
    `preSharedKeys`*: Option[Value]
    `protocol`*: Option[Value]
    `service`*: ServiceSelector
proc `$`*(x: SessionItem | NoisePreSharedKeys | Initiator | Packet |
    NoiseProtocol |
    SecretKeyField |
    NoisePathStepDetail |
    NoiseServiceSpec |
    NoiseDescriptionDetail |
    NoiseSpec): string =
  `$`(toPreserves(x))

proc encode*(x: SessionItem | NoisePreSharedKeys | Initiator | Packet |
    NoiseProtocol |
    SecretKeyField |
    NoisePathStepDetail |
    NoiseServiceSpec |
    NoiseDescriptionDetail |
    NoiseSpec): seq[byte] =
  encode(toPreserves(x))
