
import
  preserves

type
  ServiceDependency* {.preservesRecord: "depends-on".} = object
    `depender`*: Value
    `dependee`*: ServiceState
  ServiceObject* {.preservesRecord: "service-object".} = object
    `serviceName`*: Value
    `object`*: Value
  RequireService* {.preservesRecord: "require-service".} = object
    `serviceName`*: Value
  RestartService* {.preservesRecord: "restart-service".} = object
    `serviceName`*: Value
  RunService* {.preservesRecord: "run-service".} = object
    `serviceName`*: Value
  ServiceState* {.preservesRecord: "service-state".} = object
    `serviceName`*: Value
    `state`*: State
  StateKind* {.pure.} = enum
    `started`, `ready`, `failed`, `complete`, `userDefined`
  `State`* {.preservesOr.} = object
    case orKind*: StateKind
    of StateKind.`started`:
      `started`* {.preservesLiteral: "started".}: bool
    of StateKind.`ready`:
      `ready`* {.preservesLiteral: "ready".}: bool
    of StateKind.`failed`:
      `failed`* {.preservesLiteral: "failed".}: bool
    of StateKind.`complete`:
      `complete`* {.preservesLiteral: "complete".}: bool
    of StateKind.`userDefined`:
      `userdefined`*: Value
proc `$`*(x: ServiceDependency | ServiceObject | RequireService | RestartService |
    RunService |
    ServiceState |
    State): string =
  `$`(toPreserves(x))

proc encode*(x: ServiceDependency | ServiceObject | RequireService |
    RestartService |
    RunService |
    ServiceState |
    State): seq[byte] =
  encode(toPreserves(x))
