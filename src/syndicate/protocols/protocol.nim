
import
  preserves

type
  Error* {.preservesRecord: "error".} = object
    `message`*: string
    `detail`*: Value
  Turn* = seq[TurnEvent]
  Oid* = BiggestInt
  PacketKind* {.pure.} = enum
    `Nop`, `Turn`, `Error`, `Extension`
  `Packet`* {.preservesOr.} = object
    case orKind*: PacketKind
    of PacketKind.`Nop`:
      `nop`* {.preservesLiteral: "#f".}: bool
    of PacketKind.`Turn`:
      `turn`*: Turn
    of PacketKind.`Error`:
      `error`*: Error
    of PacketKind.`Extension`:
      `extension`*: Extension
  TurnEvent* {.preservesTuple.} = object
    `oid`*: Oid
    `event`*: Event
  Extension* = Value
  Message* {.preservesRecord: "M".} = object
    `body`*: Assertion
  Assertion* = Value
  Retract* {.preservesRecord: "R".} = object
    `handle`*: Handle
  Sync* {.preservesRecord: "S".} = object
    `peer`* {.preservesEmbedded.}: Value
  EventKind* {.pure.} = enum
    `Assert`, `Retract`, `Message`, `Sync`
  `Event`* {.preservesOr.} = object
    case orKind*: EventKind
    of EventKind.`Assert`:
      `assert`*: Assert
    of EventKind.`Retract`:
      `retract`*: Retract
    of EventKind.`Message`:
      `message`*: Message
    of EventKind.`Sync`:
      `sync`*: Sync
  Handle* = BiggestInt
  Assert* {.preservesRecord: "A".} = object
    `assertion`*: Assertion
    `handle`*: Handle
proc `$`*(x: Error | Turn | Oid | Packet | TurnEvent | Message | Retract | Sync |
    Event |
    Handle |
    Assert): string =
  `$`(toPreserves(x))

proc encode*(x: Error | Turn | Oid | Packet | TurnEvent | Message | Retract |
    Sync |
    Event |
    Handle |
    Assert): seq[byte] =
  encode(toPreserves(x))
