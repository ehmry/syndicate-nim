
import
  preserves, std/options

type
  HttpClientStepDetailResponseContentTypeOverride* = Option[string]
  `HttpClientStepDetail`* {.preservesDictionary.} = object
    `response-content-type-override`*: Option[string]
  HttpClientResolve* {.preservesRecord: "resolve".} = object
    `step`*: HttpClientStep
    `observer`* {.preservesEmbedded.}: EmbeddedRef
  HttpServerStep* {.preservesRecord: "http-server".} = object
    `detail`*: HttpServerStepDetail
  ResponseContentTypeOverrideDetailKind* {.pure.} = enum
    `present`, `absent`
  ResponseContentTypeOverrideDetailPresent* {.preservesDictionary.} = object
    `response-content-type-override`*: string
  ResponseContentTypeOverrideDetailAbsent* {.preservesDictionary.} = object
  `ResponseContentTypeOverrideDetail`* {.preservesOr.} = object
    case orKind*: ResponseContentTypeOverrideDetailKind
    of ResponseContentTypeOverrideDetailKind.`present`:
      `present`*: ResponseContentTypeOverrideDetailPresent
    of ResponseContentTypeOverrideDetailKind.`absent`:
      `absent`*: ResponseContentTypeOverrideDetailAbsent
  HttpServerResolve* {.preservesRecord: "resolve".} = object
    `step`*: HttpServerStep
    `observer`* {.preservesEmbedded.}: EmbeddedRef
  HttpServerStepDetail* {.preservesDictionary.} = object
  HttpClientStep* {.preservesRecord: "http-client".} = object
    `detail`*: HttpClientStepDetail
proc `$`*(x: HttpClientStepDetail | HttpClientResolve | HttpServerStep |
    ResponseContentTypeOverrideDetail |
    HttpServerResolve |
    HttpServerStepDetail |
    HttpClientStep): string =
  `$`(toPreserves(x))

proc encode*(x: HttpClientStepDetail | HttpClientResolve | HttpServerStep |
    ResponseContentTypeOverrideDetail |
    HttpServerResolve |
    HttpServerStepDetail |
    HttpClientStep): seq[byte] =
  encode(toPreserves(x))
