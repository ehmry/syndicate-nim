
import
  preserves, std/tables

type
  GroupTypeKind* {.pure.} = enum
    `rec`, `arr`, `dict`
  GroupTypeRec* {.preservesRecord: "rec".} = object
    `label`*: Value
  GroupTypeArr* {.preservesRecord: "arr".} = object
  GroupTypeDict* {.preservesRecord: "dict".} = object
  `GroupType`* {.preservesOr.} = object
    case orKind*: GroupTypeKind
    of GroupTypeKind.`rec`:
      `rec`*: GroupTypeRec
    of GroupTypeKind.`arr`:
      `arr`*: GroupTypeArr
    of GroupTypeKind.`dict`:
      `dict`*: GroupTypeDict
  AnyAtomKind* {.pure.} = enum
    `bool`, `double`, `int`, `string`, `bytes`, `symbol`, `embedded`
  `AnyAtom`* {.preservesOr.} = object
    case orKind*: AnyAtomKind
    of AnyAtomKind.`bool`:
      `bool`*: bool
    of AnyAtomKind.`double`:
      `double`*: float
    of AnyAtomKind.`int`:
      `int`*: BiggestInt
    of AnyAtomKind.`string`:
      `string`*: string
    of AnyAtomKind.`bytes`:
      `bytes`*: seq[byte]
    of AnyAtomKind.`symbol`:
      `symbol`*: Symbol
    of AnyAtomKind.`embedded`:
      `embedded`*: EmbeddedRef
  PatternKind* {.pure.} = enum
    `discard`, `bind`, `lit`, `group`
  PatternDiscard* {.preservesRecord: "_".} = object
  PatternBind* {.preservesRecord: "bind".} = object
    `pattern`*: Pattern
  PatternLit* {.preservesRecord: "lit".} = object
    `value`*: AnyAtom
  PatternGroup* {.preservesRecord: "group".} = object
    `type`*: GroupType
    `entries`*: OrderedTable[Value, Pattern]
  `Pattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: PatternKind
    of PatternKind.`discard`:
      `discard`*: PatternDiscard
    of PatternKind.`bind`:
      `bind`*: PatternBind
    of PatternKind.`lit`:
      `lit`*: PatternLit
    of PatternKind.`group`:
      `group`*: PatternGroup
proc `$`*(x: GroupType | AnyAtom | Pattern): string =
  `$`(toPreserves(x))

proc encode*(x: GroupType | AnyAtom | Pattern): seq[byte] =
  encode(toPreserves(x))
