
import
  preserves

type
  TcpRemote* {.preservesRecord: "tcp-remote".} = object
    `host`*: string
    `port`*: BiggestInt
  TcpLocal* {.preservesRecord: "tcp-local".} = object
    `host`*: string
    `port`*: BiggestInt
  TcpPeerInfo* {.preservesRecord: "tcp-peer".} = object
    `handle`* {.preservesEmbedded.}: EmbeddedRef
    `local`*: TcpLocal
    `remote`*: TcpRemote
proc `$`*(x: TcpRemote | TcpLocal | TcpPeerInfo): string =
  `$`(toPreserves(x))

proc encode*(x: TcpRemote | TcpLocal | TcpPeerInfo): seq[byte] =
  encode(toPreserves(x))
