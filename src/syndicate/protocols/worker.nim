
import
  preserves

type
  Instance* {.preservesRecord: "Instance".} = object
    `name`*: string
    `argument`*: Value
proc `$`*(x: Instance): string =
  `$`(toPreserves(x))

proc encode*(x: Instance): seq[byte] =
  encode(toPreserves(x))
