
import
  preserves, std/options, std/tables

type
  PNot* {.preservesRecord: "not".} = object
    `pattern`*: Pattern
  SturdyPathStepDetail* = Parameters
  Oid* = BiggestInt
  PDiscard* {.preservesRecord: "_".} = object
  SturdyRef* {.preservesRecord: "ref".} = object
    `parameters`*: Parameters
  TemplateKind* {.pure.} = enum
    `TAttenuate`, `TRef`, `Lit`, `TCompound`
  `Template`* {.acyclic, preservesOr.} = ref object
    case orKind*: TemplateKind
    of TemplateKind.`TAttenuate`:
      `tattenuate`*: TAttenuate
    of TemplateKind.`TRef`:
      `tref`*: TRef
    of TemplateKind.`Lit`:
      `lit`*: Lit
    of TemplateKind.`TCompound`:
      `tcompound`*: TCompound
  CaveatsFieldKind* {.pure.} = enum
    `present`, `invalid`, `absent`
  CaveatsFieldPresent* {.preservesDictionary.} = object
    `caveats`*: seq[Caveat]
  CaveatsFieldInvalid* {.preservesDictionary.} = object
    `caveats`*: Value
  CaveatsFieldAbsent* {.preservesDictionary.} = object
  `CaveatsField`* {.preservesOr.} = object
    case orKind*: CaveatsFieldKind
    of CaveatsFieldKind.`present`:
      `present`*: CaveatsFieldPresent
    of CaveatsFieldKind.`invalid`:
      `invalid`*: CaveatsFieldInvalid
    of CaveatsFieldKind.`absent`:
      `absent`*: CaveatsFieldAbsent
  PCompoundKind* {.pure.} = enum
    `rec`, `arr`, `dict`
  PCompoundRec* {.preservesRecord: "rec".} = object
    `label`*: Value
    `fields`*: seq[Pattern]
  PCompoundArr* {.preservesRecord: "arr".} = object
    `items`*: seq[Pattern]
  PCompoundDict* {.preservesRecord: "dict".} = object
    `entries`*: OrderedTable[Value, Pattern]
  `PCompound`* {.preservesOr.} = object
    case orKind*: PCompoundKind
    of PCompoundKind.`rec`:
      `rec`*: PCompoundRec
    of PCompoundKind.`arr`:
      `arr`*: PCompoundArr
    of PCompoundKind.`dict`:
      `dict`*: PCompoundDict
  SturdyDescriptionDetail* {.preservesDictionary.} = object
    `key`*: seq[byte]
    `oid`*: Value
  SturdyStepDetail* = Parameters
  `PAtom`* {.preservesOr, pure.} = enum
    `Boolean`, `Double`, `SignedInteger`, `String`, `ByteString`, `Symbol`
  WireRefKind* {.pure.} = enum
    `mine`, `yours`
  WireRefMine* {.preservesTuple.} = object
    `field0`* {.preservesLiteral: "0".}: tuple[]
    `oid`*: Oid
  WireRefYours* {.preservesTuple.} = object
    `field0`* {.preservesLiteral: "1".}: tuple[]
    `oid`*: Oid
    `attenuation`* {.preservesTupleTail.}: seq[Caveat]
  `WireRef`* {.preservesOr.} = object
    case orKind*: WireRefKind
    of WireRefKind.`mine`:
      `mine`*: WireRefMine
    of WireRefKind.`yours`:
      `yours`*: WireRefYours
  Rewrite* {.preservesRecord: "rewrite".} = object
    `pattern`*: Pattern
    `template`*: Template
  PAnd* {.preservesRecord: "and".} = object
    `patterns`*: seq[Pattern]
  CaveatKind* {.pure.} = enum
    `Rewrite`, `Alts`, `Reject`, `unknown`
  `Caveat`* {.preservesOr.} = object
    case orKind*: CaveatKind
    of CaveatKind.`Rewrite`:
      `rewrite`*: Rewrite
    of CaveatKind.`Alts`:
      `alts`*: Alts
    of CaveatKind.`Reject`:
      `reject`*: Reject
    of CaveatKind.`unknown`:
      `unknown`*: Value
  TCompoundKind* {.pure.} = enum
    `rec`, `arr`, `dict`
  TCompoundRec* {.preservesRecord: "rec".} = object
    `label`*: Value
    `fields`*: seq[Template]
  TCompoundArr* {.preservesRecord: "arr".} = object
    `items`*: seq[Template]
  TCompoundDict* {.preservesRecord: "dict".} = object
    `entries`*: OrderedTable[Value, Template]
  `TCompound`* {.preservesOr.} = object
    case orKind*: TCompoundKind
    of TCompoundKind.`rec`:
      `rec`*: TCompoundRec
    of TCompoundKind.`arr`:
      `arr`*: TCompoundArr
    of TCompoundKind.`dict`:
      `dict`*: TCompoundDict
  PatternKind* {.pure.} = enum
    `PDiscard`, `PAtom`, `PEmbedded`, `PBind`, `PAnd`, `PNot`, `Lit`,
    `PCompound`
  `Pattern`* {.acyclic, preservesOr.} = ref object
    case orKind*: PatternKind
    of PatternKind.`PDiscard`:
      `pdiscard`*: PDiscard
    of PatternKind.`PAtom`:
      `patom`*: PAtom
    of PatternKind.`PEmbedded`:
      `pembedded`* {.preservesLiteral: "Embedded".}: bool
    of PatternKind.`PBind`:
      `pbind`*: PBind
    of PatternKind.`PAnd`:
      `pand`*: PAnd
    of PatternKind.`PNot`:
      `pnot`*: PNot
    of PatternKind.`Lit`:
      `lit`*: Lit
    of PatternKind.`PCompound`:
      `pcompound`*: PCompound
  TAttenuate* {.preservesRecord: "attenuate".} = object
    `template`*: Template
    `attenuation`*: seq[Caveat]
  PBind* {.preservesRecord: "bind".} = object
    `pattern`*: Pattern
  ParametersCaveats* = Option[Value]
  ParametersOid* = Value
  ParametersSig* = seq[byte]
  `Parameters`* {.preservesDictionary.} = object
    `caveats`*: Option[Value]
    `oid`*: Value
    `sig`*: seq[byte]
  Reject* {.preservesRecord: "reject".} = object
    `pattern`*: Pattern
  Lit* {.preservesRecord: "lit".} = object
    `value`*: Value
  Alts* {.preservesRecord: "or".} = object
    `alternatives`*: seq[Rewrite]
  TRef* {.preservesRecord: "ref".} = object
    `binding`*: BiggestInt
proc `$`*(x: PNot | SturdyPathStepDetail | Oid | PDiscard | SturdyRef | Template |
    CaveatsField |
    PCompound |
    SturdyDescriptionDetail |
    SturdyStepDetail |
    WireRef |
    Rewrite |
    PAnd |
    Caveat |
    TCompound |
    Pattern |
    TAttenuate |
    PBind |
    Parameters |
    Reject |
    Lit |
    Alts |
    TRef): string =
  `$`(toPreserves(x))

proc encode*(x: PNot | SturdyPathStepDetail | Oid | PDiscard | SturdyRef |
    Template |
    CaveatsField |
    PCompound |
    SturdyDescriptionDetail |
    SturdyStepDetail |
    WireRef |
    Rewrite |
    PAnd |
    Caveat |
    TCompound |
    Pattern |
    TAttenuate |
    PBind |
    Parameters |
    Reject |
    Lit |
    Alts |
    TRef): seq[byte] =
  encode(toPreserves(x))
