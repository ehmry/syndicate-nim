
import
  preserves

type
  Answer* {.preservesRecord: "a".} = object
    `request`*: Value
    `response`*: Value
  ResultKind* {.pure.} = enum
    `ok`, `error`
  ResultOk* {.preservesRecord: "ok".} = object
    `value`*: Value
  ResultError* {.preservesRecord: "error".} = object
    `error`*: Value
  `Result`* {.preservesOr.} = object
    case orKind*: ResultKind
    of ResultKind.`ok`:
      `ok`*: ResultOk
    of ResultKind.`error`:
      `error`*: ResultError
  Question* {.preservesRecord: "q".} = object
    `request`*: Value
proc `$`*(x: Answer | Result | Question): string =
  `$`(toPreserves(x))

proc encode*(x: Answer | Result | Question): seq[byte] =
  encode(toPreserves(x))
