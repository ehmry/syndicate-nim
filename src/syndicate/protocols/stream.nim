
import
  preserves

type
  `LineMode`* {.preservesOr, pure.} = enum
    `lf`, `crlf`
  SourceKind* {.pure.} = enum
    `sink`, `StreamError`, `credit`
  SourceSink* {.preservesRecord: "sink".} = object
    `controller`* {.preservesEmbedded.}: EmbeddedRef
  SourceCredit* {.preservesRecord: "credit".} = object
    `amount`*: CreditAmount
    `mode`*: Mode
  `Source`* {.acyclic, preservesOr.} = ref object
    case orKind*: SourceKind
    of SourceKind.`sink`:
      `sink`*: SourceSink
    of SourceKind.`StreamError`:
      `streamerror`*: StreamError
    of SourceKind.`credit`:
      `credit`*: SourceCredit
  CreditAmountKind* {.pure.} = enum
    `count`, `unbounded`
  `CreditAmount`* {.preservesOr.} = object
    case orKind*: CreditAmountKind
    of CreditAmountKind.`count`:
      `count`*: BiggestInt
    of CreditAmountKind.`unbounded`:
      `unbounded`* {.preservesLiteral: "unbounded".}: bool
  StreamError* {.preservesRecord: "error".} = object
    `message`*: string
  StreamListenerError* {.preservesRecord: "stream-listener-error".} = object
    `spec`*: Value
    `message`*: string
  StreamListenerReady* {.preservesRecord: "stream-listener-ready".} = object
    `spec`*: Value
  SinkKind* {.pure.} = enum
    `source`, `StreamError`, `data`, `eof`
  SinkSource* {.preservesRecord: "source".} = object
    `controller`* {.preservesEmbedded.}: EmbeddedRef
  SinkData* {.preservesRecord: "data".} = object
    `payload`*: Value
    `mode`*: Mode
  SinkEof* {.preservesRecord: "eof".} = object
  `Sink`* {.acyclic, preservesOr.} = ref object
    case orKind*: SinkKind
    of SinkKind.`source`:
      `source`*: SinkSource
    of SinkKind.`StreamError`:
      `streamerror`*: StreamError
    of SinkKind.`data`:
      `data`*: SinkData
    of SinkKind.`eof`:
      `eof`*: SinkEof
  StreamConnection* {.preservesRecord: "stream-connection".} = object
    `source`* {.preservesEmbedded.}: EmbeddedRef
    `sink`* {.preservesEmbedded.}: EmbeddedRef
    `spec`*: Value
  ModeKind* {.pure.} = enum
    `bytes`, `lines`, `packet`, `object`
  ModePacket* {.preservesRecord: "packet".} = object
    `size`*: BiggestInt
  ModeObject* {.preservesRecord: "object".} = object
    `description`*: Value
  `Mode`* {.preservesOr.} = object
    case orKind*: ModeKind
    of ModeKind.`bytes`:
      `bytes`* {.preservesLiteral: "bytes".}: bool
    of ModeKind.`lines`:
      `lines`*: LineMode
    of ModeKind.`packet`:
      `packet`*: ModePacket
    of ModeKind.`object`:
      `object`*: ModeObject
proc `$`*(x: Source | CreditAmount | StreamError | StreamListenerError |
    StreamListenerReady |
    Sink |
    StreamConnection |
    Mode): string =
  `$`(toPreserves(x))

proc encode*(x: Source | CreditAmount | StreamError | StreamListenerError |
    StreamListenerReady |
    Sink |
    StreamConnection |
    Mode): seq[byte] =
  encode(toPreserves(x))
