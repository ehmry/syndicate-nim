
import
  preserves, protocol

type
  `LinkedTaskReleaseReason`* {.preservesOr, pure.} = enum
    `cancelled`, `normal`
  AssertionDescriptionKind* {.pure.} = enum
    `value`, `opaque`
  AssertionDescriptionValue* {.preservesRecord: "value".} = object
    `value`*: Value
  AssertionDescriptionOpaque* {.preservesRecord: "opaque".} = object
    `description`*: Value
  `AssertionDescription`* {.preservesOr.} = object
    case orKind*: AssertionDescriptionKind
    of AssertionDescriptionKind.`value`:
      `value`*: AssertionDescriptionValue
    of AssertionDescriptionKind.`opaque`:
      `opaque`*: AssertionDescriptionOpaque
  TargetedTurnEvent* {.preservesRecord: "event".} = object
    `target`*: Target
    `detail`*: TurnEvent
  NameKind* {.pure.} = enum
    `anonymous`, `named`
  NameAnonymous* {.preservesRecord: "anonymous".} = object
  NameNamed* {.preservesRecord: "named".} = object
    `name`*: Value
  `Name`* {.preservesOr.} = object
    case orKind*: NameKind
    of NameKind.`anonymous`:
      `anonymous`*: NameAnonymous
    of NameKind.`named`:
      `named`*: NameNamed
  `FacetStopReason`* {.preservesOr, pure.} = enum
    `explicitAction`, `inert`, `parentStopping`, `actorStopping`
  Oid* = Value
  ActionDescriptionKind* {.pure.} = enum
    `dequeue`, `enqueue`, `dequeueInternal`, `enqueueInternal`, `spawn`, `link`,
    `facetStart`, `facetStop`, `linkedTaskStart`
  ActionDescriptionDequeue* {.preservesRecord: "dequeue".} = object
    `event`*: TargetedTurnEvent
  ActionDescriptionEnqueue* {.preservesRecord: "enqueue".} = object
    `event`*: TargetedTurnEvent
  ActionDescriptionDequeueInternal* {.preservesRecord: "dequeue-internal".} = object
    `event`*: TargetedTurnEvent
  ActionDescriptionEnqueueInternal* {.preservesRecord: "enqueue-internal".} = object
    `event`*: TargetedTurnEvent
  ActionDescriptionSpawn* {.preservesRecord: "spawn".} = object
    `link`*: bool
    `id`*: ActorId
  ActionDescriptionLink* {.preservesRecord: "link".} = object
    `parentActor`*: ActorId
    `childToParent`*: protocol.Handle
    `childActor`*: ActorId
    `parentToChild`*: protocol.Handle
  ActionDescriptionFacetStart* {.preservesRecord: "facet-start".} = object
    `path`*: seq[FacetId]
  ActionDescriptionFacetStop* {.preservesRecord: "facet-stop".} = object
    `path`*: seq[FacetId]
    `reason`*: FacetStopReason
  ActionDescriptionLinkedTaskStart* {.preservesRecord: "linked-task-start".} = object
    `taskName`*: Name
    `id`*: TaskId
  `ActionDescription`* {.preservesOr.} = object
    case orKind*: ActionDescriptionKind
    of ActionDescriptionKind.`dequeue`:
      `dequeue`*: ActionDescriptionDequeue
    of ActionDescriptionKind.`enqueue`:
      `enqueue`*: ActionDescriptionEnqueue
    of ActionDescriptionKind.`dequeueInternal`:
      `dequeueinternal`*: ActionDescriptionDequeueInternal
    of ActionDescriptionKind.`enqueueInternal`:
      `enqueueinternal`*: ActionDescriptionEnqueueInternal
    of ActionDescriptionKind.`spawn`:
      `spawn`*: ActionDescriptionSpawn
    of ActionDescriptionKind.`link`:
      `link`*: ActionDescriptionLink
    of ActionDescriptionKind.`facetStart`:
      `facetstart`*: ActionDescriptionFacetStart
    of ActionDescriptionKind.`facetStop`:
      `facetstop`*: ActionDescriptionFacetStop
    of ActionDescriptionKind.`linkedTaskStart`:
      `linkedtaskstart`*: ActionDescriptionLinkedTaskStart
  ExitStatusKind* {.pure.} = enum
    `ok`, `Error`
  `ExitStatus`* {.preservesOr.} = object
    case orKind*: ExitStatusKind
    of ExitStatusKind.`ok`:
      `ok`* {.preservesLiteral: "ok".}: bool
    of ExitStatusKind.`Error`:
      `error`*: protocol.Error
  TurnEventKind* {.pure.} = enum
    `assert`, `retract`, `message`, `sync`, `breakLink`
  TurnEventAssert* {.preservesRecord: "assert".} = object
    `assertion`*: AssertionDescription
    `handle`*: protocol.Handle
  TurnEventRetract* {.preservesRecord: "retract".} = object
    `handle`*: protocol.Handle
  TurnEventMessage* {.preservesRecord: "message".} = object
    `body`*: AssertionDescription
  TurnEventSync* {.preservesRecord: "sync".} = object
    `peer`*: Target
  TurnEventBreakLink* {.preservesRecord: "break-link".} = object
    `source`*: ActorId
    `handle`*: protocol.Handle
  `TurnEvent`* {.preservesOr.} = object
    case orKind*: TurnEventKind
    of TurnEventKind.`assert`:
      `assert`*: TurnEventAssert
    of TurnEventKind.`retract`:
      `retract`*: TurnEventRetract
    of TurnEventKind.`message`:
      `message`*: TurnEventMessage
    of TurnEventKind.`sync`:
      `sync`*: TurnEventSync
    of TurnEventKind.`breakLink`:
      `breaklink`*: TurnEventBreakLink
  ActorId* = Value
  ActorActivationKind* {.pure.} = enum
    `start`, `turn`, `stop`
  ActorActivationStart* {.preservesRecord: "start".} = object
    `actorName`*: Name
  ActorActivationStop* {.preservesRecord: "stop".} = object
    `status`*: ExitStatus
  `ActorActivation`* {.preservesOr.} = object
    case orKind*: ActorActivationKind
    of ActorActivationKind.`start`:
      `start`*: ActorActivationStart
    of ActorActivationKind.`turn`:
      `turn`*: TurnDescription
    of ActorActivationKind.`stop`:
      `stop`*: ActorActivationStop
  FacetId* = Value
  TurnCauseKind* {.pure.} = enum
    `turn`, `cleanup`, `linkedTaskRelease`, `periodicActivation`, `delay`,
    `external`
  TurnCauseTurn* {.preservesRecord: "caused-by".} = object
    `id`*: TurnId
  TurnCauseCleanup* {.preservesRecord: "cleanup".} = object
  TurnCauseLinkedTaskRelease* {.preservesRecord: "linked-task-release".} = object
    `id`*: TaskId
    `reason`*: LinkedTaskReleaseReason
  TurnCausePeriodicActivation* {.preservesRecord: "periodic-activation".} = object
    `period`*: float
  TurnCauseDelay* {.preservesRecord: "delay".} = object
    `causingTurn`*: TurnId
    `amount`*: float
  TurnCauseExternal* {.preservesRecord: "external".} = object
    `description`*: Value
  `TurnCause`* {.preservesOr.} = object
    case orKind*: TurnCauseKind
    of TurnCauseKind.`turn`:
      `turn`*: TurnCauseTurn
    of TurnCauseKind.`cleanup`:
      `cleanup`*: TurnCauseCleanup
    of TurnCauseKind.`linkedTaskRelease`:
      `linkedtaskrelease`*: TurnCauseLinkedTaskRelease
    of TurnCauseKind.`periodicActivation`:
      `periodicactivation`*: TurnCausePeriodicActivation
    of TurnCauseKind.`delay`:
      `delay`*: TurnCauseDelay
    of TurnCauseKind.`external`:
      `external`*: TurnCauseExternal
  TurnId* = Value
  TraceEntry* {.preservesRecord: "trace".} = object
    `timestamp`*: float
    `actor`*: ActorId
    `item`*: ActorActivation
  Target* {.preservesRecord: "entity".} = object
    `actor`*: ActorId
    `facet`*: FacetId
    `oid`*: Oid
  TaskId* = Value
  TurnDescription* {.preservesRecord: "turn".} = object
    `id`*: TurnId
    `cause`*: TurnCause
    `actions`*: seq[ActionDescription]
proc `$`*(x: AssertionDescription | TargetedTurnEvent | Name | ActionDescription |
    ExitStatus |
    TurnEvent |
    ActorActivation |
    TurnCause |
    TraceEntry |
    Target |
    TurnDescription): string =
  `$`(toPreserves(x))

proc encode*(x: AssertionDescription | TargetedTurnEvent | Name |
    ActionDescription |
    ExitStatus |
    TurnEvent |
    ActorActivation |
    TurnCause |
    TraceEntry |
    Target |
    TurnDescription): seq[byte] =
  encode(toPreserves(x))
