
import
  preserves, dataspacePatterns

type
  Observe* {.preservesRecord: "Observe".} = object
    `pattern`*: dataspacePatterns.Pattern
    `observer`* {.preservesEmbedded.}: EmbeddedRef
proc `$`*(x: Observe): string =
  `$`(toPreserves(x))

proc encode*(x: Observe): seq[byte] =
  encode(toPreserves(x))
