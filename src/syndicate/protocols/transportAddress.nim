
import
  preserves

type
  Unix* {.preservesRecord: "unix".} = object
    `path`*: string
  Stdio* {.preservesRecord: "stdio".} = object
  WebSocket* {.preservesRecord: "ws".} = object
    `url`*: string
  Tcp* {.preservesRecord: "tcp".} = object
    `host`*: string
    `port`*: BiggestInt
proc `$`*(x: Unix | Stdio | WebSocket | Tcp): string =
  `$`(toPreserves(x))

proc encode*(x: Unix | Stdio | WebSocket | Tcp): seq[byte] =
  encode(toPreserves(x))
