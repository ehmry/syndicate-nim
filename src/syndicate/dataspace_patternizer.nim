# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/tables,
  pkg/npeg,
  pkg/preserves,
  pkg/preserves/pegs,
  ./patterns

type
  ParseState = object
    stack: Stack
    labels: Stack
    captureCount: int
  Stack = seq[tuple[value: Value, pos: int]]

proc shrink(stack: var Stack; n: int) = stack.setLen(stack.len - n)

template pushStack(v: Value) = state.stack.add((v, capture[0].si))

template collectEntries(result: var seq[Value]; stack: var Stack) =
  for frame in stack.mitems:
    if frame.pos > capture[0].si:
      result.add frame.value.move
  stack.shrink result.len

template discardEntries(stack: var Stack) =
  var i: int
  while i < stack.len:
    if stack[i].pos > capture[0].si:
      stack.setLen(i)
    inc i

proc parsePattern*(text: string): (Pattern, seq[Value]) =
  ## Parse a Syndicate dataspace pattern using a special pattern syntax.
  ## The parsed `Pattern` is returned with a list of annotated labels for binding.
  ## If one or more captures are not annotated with a label then an empty
  ## list of labels is returned.
  let parser = peg("Document", state: ParseState):

    Document <- *Pattern * ws * !1

    Pattern <- (Annotation | Literal | Group | Discard | Capture | Wildcard) * ws

    Annotation <- '@' * >Preserves.Value * ws * Pattern:
      # TODO: drop backtracked labels?
      state.labels.add (parsePreserves($1), capture[1].si,)

    Literal <- Preserves.Atom | Preserves.Embedded | Preserves.Set:
      pushStack parsePreserves($0)

    Group <- Record | Sequence | Dictionary

    Discard <- "#_":
      pushStack drop().embed

    Capture <- "#(" * ws * Pattern * ')':
      pushstack state.stack.pop.value.match.grab.embed
      state.captureCount.inc()

    Wildcard  <- "#?":
      pushStack grab().embed
      state.captureCount.inc()

    Embedded <- "#:" * Pattern:
      discard state.stack.pop()
      pushstack drop().toPreserves

    Record <- '<' * ws * +Pattern * '>':
      var
        pat = newPattern(GroupTypeKind.rec)
        off = -1
        i: int
      while i < state.stack.len:
        if state.stack[i].pos > capture[0].si:
          if off < 0:
            pat.group.`type`.rec.label = state.stack[i].value.move
            off = i
          else:
            pat.group.entries[toPreserves(i - off - 1)] = state.stack[i].value.move.match
        inc i
      state.stack.setLen(off)
      pushStack pat.embed

    Sequence <-  '[' * ws * *Pattern * ']':
      var
        pat = newPattern(GroupTypeKind.arr)
        off = -1
        i: int
      while i < state.stack.len:
        if state.stack[i].pos > capture[0].si:
          if off < 0: off = i
          pat.group.entries[toPreserves(i - off)] = state.stack[i].value.move.match
        inc i
      state.stack.setLen(off)
      pushStack pat.embed

    Dictionary <- '{' * *(Preserves.commas * Pattern * ':' * Pattern) * Preserves.commas * '}':
      var pat = newPattern(GroupTypeKind.dict)
      for i in countDown(state.stack.high.pred, 0, 2):
        if state.stack[i].pos < capture[0].si: break
        var
          val = state.stack.pop.value
          key = state.stack.pop.value
        validate(key notin pat.group.entries)
        pat.group.entries[key] = val.match
      pushStack pat.embed

    ws <- *{ ' ', '\t', '\r', '\n' }

  var state: ParseState
  let match = parser.match(text, state)
  if not match.ok:
    raise newException(ValueError, "failed to parse Preserves pattern:\n" & text[match.matchMax..text.high])
  result[0] = state.stack.pop.value.match
  if state.captureCount == state.labels.len:
    result[1].setLen state.labels.len
    for i, e in state.labels:
      result[1][i] = e.value

when isMainModule:
  import std/streams
  let
    (pattern, labels) = stdin.readAll.parsePattern
    output = newFileStream(stdout)
  output.writeText(pattern.toPreserves)
  for s in labels:
    output.write('\0')
    output.writeText(s)
  output.writeLine()
