# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

{.push raises: [].}

import
  std/tables,
  pkg/preserves,
  ./actors

proc newDuringEntity*[T](facet: Facet; cb: proc (turn: Turn; x: T) {.closure.}): Cap =
  var assertionMap = initTable[Handle, Facet]()

  proc a(turn: Turn; v: Value; h: Handle) =
    doAssert(not assertionMap.hasKey(h), "during: duplicate handle published")
    turn.withNewFacet:
      turn.facet.preventInertCheck() # Keep the facet alive until retraction.
      assertionMap[h] = turn.facet
      when T is Value:
        cb(turn, v)
      else:
        var x: T
        if x.fromPreserves(v):
          cb(turn, x)

  proc r(turn: Turn; h: Handle) =
    var child: Facet
    if assertionMap.pop(h, child):
      turn.withFacet child: turn.stopFacet()

  facet.newCap(facet.newEntity(a, r))
