# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[streams, strutils, tables, xmltree],
  pkg/preserves,
  ./protocols/trace

type DrawMode = enum drawActor, drawEffects

proc prTxt[T](v: T): string =
  strutils.escape ($v.toPreserves)

func toNodeId(name: trace.Name): string =
  case name.orKind
    of NameKind.anonymous:
      "anonymous"
    of NameKind.named:
      name.named.name.prTxt

proc addColums[T](row: XmlNode; x: T) =
  let pr = x.toPreserves
  row.add <>td(newText($pr.label))
  for field in pr.items:
    row.add <>td(align="left", newText($field))

proc toTable(actor: string; turn: TurnDescription): string =
  let
    table = newElement("table")
    emptyData = <>td(newText(""))
  table.add <>tr(
      <>td(port="id", <>b(newText("Turn " & $turn.id))),
      <>td(newText($turn.cause)))
  table.attrs = { "border": "0", "cellborder": "1", "cellspacing": "0"}.toXmlAttributes
  for act in turn.actions:
    let
      row = newElement("tr")
      actPr = act.toPreserves
    let hdr = <>td(newText($actPr.label))
    case act.orKind
    of dequeue:
      hdr.attrs = { "bgcolor": "palegreen" }.toXmlAttributes
    of dequeueinternal:
      hdr.attrs = { "bgcolor": "lightgray" }.toXmlAttributes
    of enqueue:
      hdr.attrs = { "bgcolor": "khaki" }.toXmlAttributes
    of spawn:
      hdr.attrs = { "port": "Spawn_" & $act.spawn.id }.toXmlAttributes
    else: discard
    row.add hdr
    for field in actPr[0].items:
      var txt = $field
      if txt.len > 128:
        txt.setLen 127
        txt.add "…"
      row.add <>td(align="left", txt.newText)
    table.add row
  table.add <>tr(<>td(port="end", newText""))
  $table

proc writeActorTrace(str: Stream; trace: openarray[TraceEntry]; i: int; mode: DrawMode) =
  let
    actorId = trace[i].actor
    actorName = $actorId
    startNode = "Start_" & actorName
  var
    actorLabel: string
    prevNode: string
    thisNode = startNode
  if mode == drawActor:
    str.writeLine("subgraph cluster_$# {" % [actorName])
    case trace[i].item.start.actorName.orKind
    of NameKind.named:
      actorLabel = $trace[i].item.start.actorName.named.name
      str.writeLine("$# [shape=Mdiamond, label=$#]" % [thisNode, actorLabel])
    of NameKind.anonymous:
      str.writeLine("$# [shape=Mdiamond, fill=grey]" % [thisNode])
  for e in trace[i.succ .. trace.high]:
    if e.actor == actorId:
      case e.item.orKind

      of ActorActivationKind.turn:
        prevNode = move thisNode
        let turn = e.item.turn
        let turnName = "Turn_$#" % [ $turn.id ]
        thisNode = turnName
        str.writeLine("$# [shape=plain, label=<$#>]" % [
          thisNode, $toTable(actorLabel, turn)])
        case turn.cause.orKind
        of TurnCauseKind.turn:
          let causeId = "Turn_" & $turn.cause.turn.id
          if causeId != prevNode and mode == drawEffects:
            str.writeLine("$#:end -> Turn_$#:id [style=dashed]" % [
                causeId, $turn.id])
        else:
          discard
        for act in turn.actions:
          if act.orKind == spawn and mode == drawEffects:
            str.writeLine("$1:Spawn_$2 -> Start_$2 [style=dashed]" % [
                thisNode, $act.spawn.id])
        if mode == drawActor:
          let format =
            if prevNode == startNode: "$# -> $#:id"
            else: "$#:end -> $#:id"
          str.writeLine(format % [prevNode, thisNode])

      of ActorActivationKind.stop:
        if mode == drawActor:
          str.writeLine("Stop_$1 [shape=Msquare, label=$2]" % [
              actorName, e.item.stop.status.prTxt])

      of ActorActivationKind.start:
        raiseAssert "actor " & actorName & " started twice"

  if mode == drawActor:
    str.writeLine("$#:end -> Stop_$#\n}" % [thisNode, actorName])

proc renderDotGraph*(str: Stream; trace: openarray[TraceEntry]) =
  ## Render a `TraceEnty` sequence to the Graphviz DOT language.
  str.writeLine "digraph { compound=true; start=1;"
  for i, e in trace:
    if e.item.orKind == ActorActivationKind.start:
      str.writeActorTrace(trace, i, drawActor)
  for i, e in trace:
    if e.item.orKind == ActorActivationKind.start:
      str.writeActorTrace(trace, i, drawEffects)
  str.writeLine "}"

when isMainModule:
  let
    output = stdout.newFileStream()
    input = stdin.newFileStream()
  var entries: seq[TraceEntry]
  while not input.atEnd:
    var entry: TraceEntry
    doAssert entry.fromPreserves input.decodePreserves()
    entries.add entry
  output.renderDotGraph entries
  input.close()
  output.close()
