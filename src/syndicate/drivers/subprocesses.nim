# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## UNIX subprocess driver.
##
## Execute subprocesses and exchange data over stdio using messages.

import
  std/[envvars, options, oserrors, osproc, strtabs],
  pkg/sys/[files, handles, ioqueue],
  pkg/preserves,
  ../../syndicate,
  ../gatekeepers,
  ../protocols/[gatekeeper, subprocesses]

export subprocesses

from std/posix import Pid, fcntl, fsync, F_GETFL, F_SETFL, O_NONBLOCK
from std/posix_utils import sendSignal

proc reopenAsync(fd: FileHandle): AsyncFile =
  var flags =  fcntl(fd.cint, F_GETFL, 0)
  if flags < 0:
    raiseOSError(osLastError())
  if fcntl(fd.cint, F_SETFL, flags or O_NONBLOCK) < 0:
    raiseOSError(osLastError())
  newAsyncFile(FD fd)

type
  SubprocessEntity {.final.} = ref object of Entity
    process: Process
    stdioFileStreams: array[3, FileStream]
    stdinSyncList: seq[Cap]
    decoder: BufferedDecoder

  FileStream = tuple
    file: AsyncFile
    proto: StreamProtocol

  StreamProtocol = enum
    protoBytes = "application/octet-stream",
    protoText = "text/plain",
    protoJson = "application/json",
    protoPreserves = "application/preserves"

const
  stdinId = 0
  stdoutId = 1
  stderrId = 2

proc consume(sub: SubprocessEntity; buf: seq[byte]) {.asyncio.} =
  let syncList = move sub.stdinSyncList
  if sub.stdioFileStreams[stdinId].file.fd != InvalidFD:
    var n = sub.stdioFileStreams[stdinId].file.write(buf)
    if n == 0 or n != buf.len:
      assert n <= 0
      sub.stdioFileStreams[stdinId].file.close()
      queueTurn(sub.facet, externalCause "stdin closed", stopFacet)
  if syncList.len > 0:
    proc syncAction(turn: Turn) {.closure.} =
      for peer in syncList:
        message(turn, peer, true)
    queueTurn(sub.facet, externalCause "sync", syncAction)

proc subprocessMessage(turn: Turn; e: Entity; v: Value) =
  let sub = SubprocessEntity(e)
  var ctrl: ControlMessageForSubprocess
  if ctrl.fromPreserves(v):
    case ctrl.orKind
    of ControlMessageForSubprocessKind.EOF:
      discard trampoline:
        whelp sub.consume(newSeq[byte]())
          # queue a write so that the buffer clears before closing
    of ControlMessageForSubprocessKind.Signal:
      sub.process.processID.Pid.sendSignal(ctrl.signal.num)
  else:
    case sub.stdioFileStreams[stdinId].proto
    of protoBytes, protoText:
      if v.isByteString and v.bytes.len > 0:
        discard trampoline:
          whelp sub.consume(v.bytes)
      if v.isString and v.string.len > 0:
        discard trampoline:
          whelp sub.consume(cast[seq[byte]](v.string))
    of protoJson:
      let txt = v.jsonText
      discard trampoline:
        whelp sub.consume(cast[seq[byte]](txt))
    of protoPreserves:
      discard trampoline:
        whelp sub.consume(v.encode)

proc subProcessSync(turn: Turn; e: Entity; peer: Cap) =
  SubprocessEntity(e).stdinSyncList.add(peer)

proc sendData(sub: SubprocessEntity; dest: Cap; buf: ref seq[byte]; len: int; proto: StreamProtocol) =
  case proto
  of protoBytes, protoText:
    var v =
      if proto == protoBytes: buf[][0..<len].toPreserves
      else: cast[string](buf[][0..<len]).toPreserves
    queueTurn(sub.facet, externalCause "read from subprocess") do (turn: Turn):
      message(turn, dest, v)
  of protoJson, protoPreserves:
    sub.decoder.feed(buf[][0].addr, len)
    var v: Option[Value]
    if proto == protoPreserves:
      v = sub.decoder.decode()
    if v.isNone:
      v = sub.decoder.parse()
    if v.isSome:
      queueTurn(sub.facet, externalCause "read from subprocess") do (turn: Turn):
        message(turn, dest, v.get)

proc readLoop(sub: SubprocessEntity; dest: Cap; id: int) {.asyncio.} =
  assert not sub.stdioFileStreams[id].file.isNil
  let buf = new seq[byte]
  while sub.stdioFileStreams[id].file.fd != InvalidFD:
    buf[].setLen(0x1000)
    let n = sub.stdioFileStreams[id].file.read(buf)
    if n <= 0:
      sub.stdioFileStreams[id].file.close()
    elif not dest.isNil:
      sub.sendData(dest, buf, n, sub.stdioFileStreams[id].proto)
  if not dest.isNil:
    proc sendEof(turn: Turn) {.closure.} =
      message(turn, dest, Eof())
      if not sub.process.running:
        publish(turn, dest, Exit(detail: sub.process.peekExitCode.toPreserves))
    queueTurn(sub.facet, externalCause "EOF", sendEof)

proc toProtocol(v: Value): StreamProtocol =
  if v.isSymbol:
    case $v
    of "text/plain", "text":
      result = protoText
    of "application/json", "json":
      result = protoJson
    of "application/preserves", "preserves":
      result = protoPreserves
    else:
      discard

proc linkSubprocessAdapter*(turn: Turn; detail: SubprocessAdapterDetail; obs: Cap): Actor {.discardable.} =
  ## Spawn an actor that starts a UNIX subprocess.
  var
    command: string
    workingDir: string
    args: seq[string]
    env = newStringTable()
    options: set[ProcessOption]
    stderrCap: Cap

  case detail.argv.orKind
  of CommandLineKind.shell:
    command = detail.argv.shell
    options.incl(poEvalCommand)
  of CommandLineKind.full:
    command = detail.argv.full.program
    args = detail.argv.full.args

  if detail.dir.isSome and detail.dir.get.isString:
    workingDir = detail.dir.get.string

  if detail.clearEnv.isNone or detail.clearEnv.get.isFalse:
    for key, val in envPairs():
      env[key] = val

  if detail.env.isSome and detail.env.get.isDictionary:
    for key, val in detail.env.get.pairs:
      if key.isString:
        if val.isString:
          env[key.string] = val.string
        elif val.isFalse:
          env.del(key.string)
        # discard otherwise

  var sub = SubprocessEntity(m: subprocessMessage)
  sub.process = startProcess(command, workingDir, args, env, options)
  if not sub.process.running:
    raise newException(CatchableError, "failed to start process")
  sub.stdioFileStreams = [
      (sub.process.inputHandle.reopenAsync, protoBytes),
      (sub.process.outputHandle.reopenAsync, protoBytes),
      (sub.process.errorHandle.reopenAsync, protoText),
    ]

  if detail.`stdin-protocol`.isSome:
    sub.stdioFileStreams[stdinId].proto = detail.`stdin-protocol`.get.toProtocol

  if detail.`stdout-protocol`.isSome:
    sub.stdioFileStreams[stdoutId].proto = detail.`stdout-protocol`.get.toProtocol
    if sub.stdioFileStreams[stdoutId].proto != protoBytes:
      sub.decoder = newBufferedDecoder()

  result = linkActor(turn, command) do (turn: Turn):
    sub.facet = turn.facet
      # TODO: there should be a cleaner way to return a Cap owned by a fresh actor.
    discard trampoline:
      assert not detail.stdout.Cap.isNil
      whelp sub.readLoop(detail.stdout.Cap, stdoutId)
    if detail.stderr.isSome:
      let opt = detail.stderr.get.unembed(Cap)
      if opt.isSome:
        stderrCap = opt.get
    discard trampoline:
      whelp sub.readLoop(stderrCap, stderrId)
    publish(turn, obs, ResolvedAccepted(responderSession: sub.newCap))

proc spawnSubprocessAdapterGatekeeper*(turn: Turn; relay: Cap) =
  during(turn, relay, Resolve?:{ 0: grabType(SubprocessAdapterStep), 1: grab() }
      ) do (step: SubprocessAdapterStep, obs: Cap):
    try:
      discard linkSubprocessAdapter(turn, step.detail, obs)
    except CatchableError as err:
      discard publish(turn, obs, Rejected(detail: err.msg.toPreserves))

when isMainModule:
  import ../relays
  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      spawnSubprocessAdapterGatekeeper(turn, relay)
