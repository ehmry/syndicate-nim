# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## HTTP server and client drivers.

import
  std/[httpcore, parseutils, sets, streams, strutils, tables, times, uri],
  pkg/taps,
  pkg/preserves,
  ../../syndicate, ../bags, ./timers, ../protocols/[gatekeeper, http, http_driver]

export http, http_driver

const
  CRLF = "\x0d\x0a"
  SP = { ' ', '\x09', '\x0b', '\x0c', '\x0d' }
  SupportedVersion = "HTTP/1.1"
  IMF = initTimeFormat"ddd, dd MMM yyyy HH:mm:ss"

when defined(posix):
  proc echo(args: varargs[string, `$`]) {.used.} =
    stderr.writeLine(args)

type
  Driver = ref object
    facet: Facet
    ds, timers: Cap
    sequenceNumber: BiggestInt
  Session = ref object
    facet: Facet
    driver: Driver
    conn: Connection
    exch: Exchange
    pendingLen: int
    port: Port
  Exchange = ref object
    facet: Facet
    cap: Cap
    ses: Session
      # TODO: store what we need fom Session locally.
    req: HttpRequest
    binding: Option[HttpBinding]
    stream: StringStream
    contentType: string
    contentLen: int
    mode: HttpResponseKind
    active: bool

proc badRequest(conn: Connection; msg: string) =
  conn.send(SupportedVersion & " " & msg, endOfMessage = true)

proc extractQuery(s: var string): Table[Symbol, seq[QueryValue]] =
  let start = succ skipUntil(s, '?')
  if start < s.len:
    var query = s[start..s.high]
    s.setLen(pred start)
    for key, val in uri.decodeQuery(query):
      var list = result.getOrDefault(Symbol key)
      list.add QueryValue(orKind: QueryValueKind.string, string: val)
      result[Symbol key] = list

proc parseRequest(conn: Connection; exch: Exchange; text: string): int =
  ## Parse an `HttpRequest` request out of a `text` from a `Connection`.
  var
    token: string
    off: int

  template advanceSp =
    let n = skipWhile(text, SP, off)
    if n < 1:
      badRequest(conn, "400 invalid request")
      return
    inc(off, n)

  # method
  off.inc parseUntil(text, token, SP, off)
  exch.req.method = token.toLowerAscii.Symbol
  advanceSp()

  # target
  if text[off] == '/': inc(off) #TODO: always a leading slash?
  off.inc parseUntil(text, token, SP, off)
  advanceSp()

  block:
    var version: string
    off.inc parseUntil(text, version, SP, off)
    advanceSp()
    if version != SupportedVersion:
      badRequest(conn, "400 version not supported")
      return

  exch.req.query = extractQuery(token)

  if token != "":
    exch.req.path = split(token, '/')
    for p in exch.req.path.mitems:
      # normalize the path
      for i, c in p:
        if c in {'A'..'Z'}:
          p[i] = char c.ord + 0x20

  exch.req.host = RequestHost(orKind: RequestHostKind.absent)

  template advanceLine =
    inc off, skipWhile(text, {'\x0d'}, off)
    if text.high < off or text[off] != '\x0a':
      badRequest(conn, "400 invalid request")
      return
    inc off, 1

  advanceLine()
  while off < text.len:
    off.inc parseUntil(text, token, {'\x0d', '\x0a'}, off)
    if token == "": break
    advanceLine()
    var
      (key, vals) = httpcore.parseHeader(token)
      k = key.toLowerAscii
      v = exch.req.headers.getOrDefault(cast[Symbol](k))
    for e in vals.mitems:
      var e = e.move.toLowerAscii
      case k
      of "host":
        exch.req.host = RequestHost(orKind: RequestHostKind.`present`, present: e)
      of "content-length":
        discard parseInt(e, exch.contentLen)
        if exch.contentLen > (1 shl 23):
          badRequest(conn, "413 Content Too Large")
        if exch.contentLen > 0:
          exch.req.body = Value(
              kind: pkByteString, bytes: newSeqOfCap[byte](exch.contentLen))
      of "content-type":
        exch.contentType = e
      if v == "":
        v = e.toLowerAscii
      else:
        v.add ", "
        v.add e.toLowerAscii
      exch.req.headers[cast[Symbol](k)] = v
  advanceLine()
  result = off

proc len(chunk: Chunk): int =
  case chunk.orKind
  of ChunkKind.string: chunk.string.len
  of ChunkKind.bytes: chunk.bytes.len

proc lenLine(chunk: Chunk): string =
  result = chunk.len.toHex.strip(true, false, {'0'})
  result.add CRLF

proc send[T: byte|char](ses: Session; data: openarray[T]) =
  ses.conn.send(addr data[0], data.len, endOfMessage = false)

proc send(ses: Session; chunk: Chunk) =
  case chunk.orKind
  of ChunkKind.string:
    ses.send(chunk.string)
  of ChunkKind.bytes:
    ses.send(chunk.bytes)

func isTrue(v: Value): bool = v.kind == pkBoolean and v.bool

proc dispatch(exch: Exchange; turn: Turn; res: HttpResponse) =
  case res.orKind
  of HttpResponseKind.status:
    if exch.mode == res.orKind:
      exch.active = true
      exch.ses.conn.startBatch()
      exch.stream.write(
          SupportedVersion, " ", res.status.code, " ", res.status.message, CRLF &
          # "connection: close" & CRLF &
            # RFC9112 says we SHOULD support persistent connections.
          "date: ", now().format(IMF), CRLF
            # add Date header automatically - RFC 9110 Section 6.6.1.
        )
      exch.mode = HttpResponseKind.header

  of HttpResponseKind.header:
    if exch.mode == res.orKind:
      exch.stream.write(res.header.name, ": ", res.header.value, CRLF)

  of HttpResponseKind.chunk:
    if exch.mode in {HttpResponseKind.header, HttpResponseKind.chunk} and res.chunk.chunk.len > 0:
      if exch.mode == HttpResponseKind.header:
        exch.stream.write("transfer-encoding: chunked" & CRLF & CRLF)
        exch.ses.send(move exch.stream.data)
        exch.mode = res.orKind
      else:
        exch.ses.conn.startBatch()
      exch.ses.send(res.chunk.chunk.lenLine)
      exch.ses.send(res.chunk.chunk)
      exch.ses.send(CRLF)
      exch.ses.conn.endBatch()

  of HttpResponseKind.done:
    if exch.mode in {HttpResponseKind.header, HttpResponseKind.chunk}:
      exch.ses.conn.startBatch()
      if exch.mode == HttpResponseKind.header:
        exch.stream.write("content-length: ", $res.done.chunk.len & CRLF & CRLF)
        exch.ses.send(move exch.stream.data)
        if res.done.chunk.len > 0:
          exch.ses.send(res.done.chunk)
      elif exch.mode == HttpResponseKind.chunk:
        exch.ses.send(res.done.chunk.lenLine)
        if res.done.chunk.len > 0:
          exch.ses.send(res.done.chunk)
        exch.ses.send(CRLF & "0" & CRLF & CRLF)
      exch.mode = res.orKind
      exch.ses.conn.endBatch()
      if exch.req.headers.getOrDefault(Symbol"connection") == "close":
        exch.ses.conn.close()
    stopFacet(turn)
      # stop the facet scoped to the exchange
      # so that the response capability is withdrawn

proc scheduleTimeout(exch: Exchange; turn: Turn) =
  const timeout = initDuration(seconds = 4)
  after(turn, exch.ses.driver.timers, timeout) do (turn: Turn):
    if not exch.active:
      var res = HttpResponse(orKind: HttpResponseKind.status)
      res.status.code = 504
      res.status.message = "Binding timeout"
      exch.dispatch(turn, res)
      res = HttpResponse(orKind: HttpResponseKind.done)
      exch.dispatch(turn, res)
        # Use a HttpResponse to reuse logic.

proc newExchange(): Exchange = new result

proc newCap(facet: Facet; exch: Exchange): Cap =
  proc m(turn: Turn; v: Value) =
    # Send responses back into a connection.
    if v.isTrue:
      # Sync message from dataspace.
      exch.scheduleTimeout(turn)
      if exch.binding.isSome:
        # The best binding in the dataspace has been selected.
        let handler = exch.binding.get.handler.unembed(Cap)
        if handler.isSome:
          # Send the handler the exchange capability.
          if exch.contentType.startsWith "application/json":
            if exch.req.body.isByteString:
              var bodyBytes = exch.req.body.bytes.move
              exch.req.body = cast[string](bodyBytes).parsePreserves
          discard publish(turn, handler.get, HttpContext(
              req: exch.req,
              res: exch.cap.embed,
            ))
        else:
          exch.binding.reset()
    else:
      var res: HttpResponse
      if exch.mode != HttpResponseKind.done and res.fromPreserves v:
        exch.dispatch(turn, res)
  facet.newCap(facet.newEntity(message=m))

func `==`(s: string; rh: RequestHost): bool =
  rh.orKind == RequestHostKind.present and rh.present == s

proc match(b: HttpBinding, r: HttpRequest): bool =
  ## Check if `HttpBinding` `b` matches `HttpRequest` `r`.
  result =
    (b.port == r.port) and
    (b.host.orKind == HostPatternKind.any or
      b.host.host == r.host) and
    (b.method.orKind == MethodPatternKind.any or
        b.method.specific == r.method)
  if result:
    for i, p in b.path:
      case p.orKind
      of PathPatternElementKind.wildcard: discard
      of PathPatternElementKind.label:
        if i > r.path.high or p.label != r.path[i]:
          return false
      of PathPatternElementKind.rest:
        return i == b.path.high
          # return false if "..." isn't the last element

proc strongerThan(a, b: HttpBinding): bool =
  ## Check if `a` is a stronger `HttpBinding` than `b`.
  result =
    (a.host.orKind != b.host.orKind and
      a.host.orKind == HostPatternKind.host) or
    (a.method.orKind != b.method.orKind and
      a.method.orKind == MethodPatternKind.specific)
  if not result:
    if a.path.len > b.path.len: return true
    for i in b.path.low..a.path.high:
      if a.path[i].orKind != b.path[i].orKind and
          a.path[i].orKind == PathPatternElementKind.label:
        return true

proc service(turn: Turn; exch: Exchange) =
  ## Service an HTTP message exchange.
  let pat = HttpBinding.match({1: ?exch.req.port}).grab()
  onPublish(turn, exch.ses.driver.ds, pat) do (b: HttpBinding):
    if b.match exch.req:
      if exch.binding.isNone or b.strongerThan exch.binding.get:
        exch.binding = some b
          # found a better binding
  exch.cap =  turn.facet.newCap(exch)
  sync(turn, exch.ses.driver.ds, exch.cap)
    # When the sync returns we have seen all the bindings.

proc exchange(ses: Session) =
  ## Detach the current Exchange from the session
  ## and pass it into Syndicate.
  ses.pendingLen = 0
  var exch = move ses.exch
  exch.ses = ses
  queueTurn(ses.facet, externalCause "HTTP connection from client") do (turn: Turn):
    turn.withNewFacet:
      # start a new facet for this message exchange
      turn.facet.preventInertCheck()
      exch.facet = turn.facet
      exch.stream = newStringStream()
      exch.mode = HttpResponseKind.status
      turn.service(exch)

proc service(ses: Session) =
  ## Service a connection to an HTTP client.
  const oneMiB = 1 shl 20
  ses.facet.onStop do (turn: Turn):
    close ses.conn
  ses.conn.onClosed do ():
    queueTurn(ses.facet, externalCause "HTTP connection closed", stopFacet)
  ses.conn.onReceivedPartial do (data: seq[byte]; ctx: MessageContext; eom: bool):
    if ses.pendingLen == 0:
      assert not ses.exch.isNil
      ses.exch.req.body = Value(kind: pkByteString)
      let off = parseRequest(ses.conn, ses.exch, cast[string](data))
      if off > 0:
        assert not ses.exch.isNil
        inc(ses.driver.sequenceNumber)
        ses.exch.req.sequenceNumber = ses.driver.sequenceNumber
        ses.exch.req.port = BiggestInt ses.port
        ses.pendingLen = ses.exch.contentLen
        if off < data.len:
          let n = min(data.len - off, ses.pendingLen)
          ses.exch.req.body.bytes.add data[off .. off+n.pred]
          ses.pendingLen.dec n
    else:
      let n = min(data.len, ses.pendingLen)
      ses.exch.req.body.bytes.add data[0..n.pred]
      ses.pendingLen.dec n
    assert ses.pendingLen >= 0, $ses.pendingLen
    if ses.pendingLen == 0:
      ses.exchange()
      ses.conn.receive(maxLength = oneMiB)
    else:
      ses.conn.receive(maxLength=ses.pendingLen)
  ses.conn.receive(maxLength =oneMiB)

proc newListener(port: Port): Listener =
  var lp = newLocalEndpoint()
  lp.with port
  listen newPreconnection(local=[lp])

proc httpListen(turn: Turn; driver: Driver; port: Port) =
  ## Start an HTTP listener at the driver.
  ## When connections come in we
  ## match bindings.
  let facet = turn.facet
  var listener = newListener(port)
  turn.facet.preventInertCheck()
  listener.onListenError do (err: ref Exception):
    queueTurn(facet, externalCause "listen error", stopFacet)
  facet.onStop do (turn: Turn):
    stop listener
  echo "listening for HTTP on port ", port.int
  listener.onConnectionReceived do (conn: Connection):
    queueTurn(driver.facet, externalCause "connection received") do (turn: Turn):
      # start a new turn
      linkActor(turn, "http-conn") do (turn: Turn):
        turn.facet.preventInertCheck()
        let facet = turn.facet
        conn.onConnectionError do (err: ref Exception):
          facet.actor.fail("connection error", err)
        # facet is scoped to the lifetime of the connection
        service Session(
            facet: turn.facet,
            driver: driver,
            exch: newExchange(),
            conn: conn,
            port: port,
          )

proc newHttpServerFacet*(turn: Turn; resolve: HttpServerResolve): Facet {.discardable.} =
  ## Create a new `Facet` that resolves an `HttpServerResolve` assertion.
  turn.withNewFacet:
    result = turn.facet
    let driver = Driver(
        facet: result,
        ds: result.newDataspace(),
        timers: result.newDataspace(),
      )
    spawnTimerDriver(turn, driver.timers)
    during(turn, driver.ds, HttpBinding.match({ 1: grab() })) do (port: BiggestInt):
      # Take only the port number from bindings.
      # The bindings will be queried later during HTTP transactions.
      publish(turn, driver.ds, HttpListener(port: port))
    during(turn, driver.ds, HttpListener.grabWithin) do (port: uint16):
      httpListen(turn, driver, Port port)
    publish(turn, resolve.observer.Cap, ResolvedAccepted(responderSession: driver.ds))

proc spawnHttpServerResolver*(turn: Turn; relay: Cap): Actor {.discardable.} =
  ## Spawn a new `Actor` that resolves `HttpServerResolve` assertions.
  result = spawnActor(turn, "http-server") do (turn: Turn):
    during(turn, relay, HttpServerResolve.grabType) do (resolve: HttpServerResolve):
        linkActor(turn, "session") do (turn: Turn):
          newHttpServerFacet(turn, resolve)

when defined(posix):
  import std/httpclient
    # TODO: write a client from scratch so that
    # it integrates with the scheduleing model

  proc url(req: HttpRequest): Uri =
    doAssert req.host.orKind == RequestHostKind.present
    doAssert req.host.present.len > 0
    result.scheme = if req.port == 80: "http" else: "https"
    result.hostname = req.host.present
    result.port = $req.port
    for i, p in req.path:
      if 0 < i: result.path.add '/'
      result.path.add p.encodeUrl
    for key, vals in req.query:
      if result.query.len > 0:
        result.query.add '&'
      result.query.add key.string.encodeUrl
      for i, val in vals:
        if i == 0: result.query.add '='
        elif i < vals.high: result.query.add ','
        result.query.add val.string.encodeUrl

  proc toContent(body: Value; contentType: var string): string =
    if not body.isFalse:
      case contentType
      of "application/json", "text/javascript":
        var stream = newStringStream()
        writeText(stream, body, textJson)
        return stream.data.move
      of "application/preserves":
        return cast[string](body.encode)
      of "text/preserves":
        return $body
      else:
        discard

      case body.kind
      of pkString:
        result = body.string
        if contentType == "":
          contentType = "text/plain"
      of pkByteString:
        result = cast[string](body.bytes)
        if contentType == "":
          contentType = "application/octet-stream"
      else:
        raise newException(ValueError, "unknown content type for body " & $body)

  proc newHttpClientFacet*(turn: Turn; resolve: HttpClientResolve): Facet {.discardable.} =
    ## Create a new `Facet` that resolves an `HttpClientResolve` assertion.
    turn.withNewFacet:
      result = turn.facet
      let ds = turn.facet.newDataspace()
      discard publish(turn, resolve.observer.Cap, ResolvedAccepted(responderSession: ds))
      during(turn, ds, HttpContext.grabType) do (ctx: HttpContext):
        stderr.writeLine ctx
        let peer = ctx.res.unembed(Cap).get
        var client = newHttpClient()
        try:
          var
            headers = newHttpHeaders()
            contentType: string
          for key, val in ctx.req.headers:
            if key == Symbol"content-type" or key == Symbol"Content-Type":
              contentType = val
            client.headers[key.string] = val
          let stdRes = client.request(
              ctx.req.url,
              ctx.req.method.string.toUpper,
              ctx.req.body.toContent(contentType), headers
            )
          var resp = HttpResponse(orKind: HttpResponseKind.status)
          resp.status.code = stdRes.status[0 .. 2].parseInt
          resp.status.message = stdRes.status[4 .. ^1]
          message(turn, peer, resp)
          resp = HttpResponse(orKind: HttpResponseKind.header)
          for key, vals in stdRes.headers.table:
            for val in vals.items:
              resp.header.name = key.Symbol
              resp.header.value = val
              message(turn, peer, resp)
          if resolve.step.detail.`response-content-type-override`.isSome:
            contentType = resolve.step.detail.`response-content-type-override`.get
          else:
            for val in stdRes.headers.table.getOrDefault("content-type").items:
              contentType = val
          case contentType
          of "application/json", "text/preserves", "text/javascript":
            message(turn, peer,
                initRecord("done", stdRes.bodyStream.readAll.parsePreserves))
          of "application/preserves":
            message(turn, peer,
                initRecord("done", stdRes.bodyStream.decodePreserves))
          else:
            resp = HttpResponse(orKind: HttpResponseKind.done)
            if contentType.startsWith "text":
              resp.done.chunk = Chunk(
                  orKind: ChunkKind.string,
                  string: stdRes.bodyStream.readAll(),
                )
            else:
              resp.done.chunk = Chunk(
                  orKind: ChunkKind.bytes,
                  bytes: cast[seq[byte]](stdRes.bodyStream.readAll()),
                )
            message(turn, peer, resp)
        except CatchableError as err:
          var resp = HttpResponse(orKind: HttpResponseKind.status)
          resp.status.code = 400
          resp.status.message = "Internal client error"
          message(turn, peer, resp)
          resp = HttpResponse(orKind: HttpResponseKind.done)
          resp.done.chunk.string = err.msg
          message(turn, peer, resp)
        client.close()
      do:
        client.close()

  proc spawnHttpClientResolver*(turn: Turn; relay: Cap): Actor {.discardable.} =
    ## Spawn a new `Actor` that resolves `HttpClientResolve` assertions.
    result = spawnActor(turn, "http-client") do (turn: Turn):
      during(turn, relay, HttpClientResolve.grabType) do (resolve: HttpClientResolve):
        linkActor(turn, "session") do (turn: Turn):
          newHttpClientFacet(turn, resolve)

when isMainModule:
  import ../relays

  when defined(solo5):
    import solo5
    acquireDevices([("eth0", netBasic)], netAcquireHook)

  runActor("main") do (turn: Turn):
    resolveEnvironment(turn) do (turn: Turn; relay: Cap):
      spawnHttpServerResolver(turn, relay)
      when not defined(solo5):
        spawnHttpClientResolver(turn, relay)
