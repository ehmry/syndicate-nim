# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

{.push raises: [].}

import
  std/[assertions, sequtils, tables, typetraits],
  pkg/preserves,
  pkg/preserves/sugar,
  ./protocols/[dataspacePatterns, dataspace]

export dataspacePatterns

type
  EmbeddedPattern* = ref object of EmbeddedRef
    ## Type for embedding a `Pattern` in a Preserves `Value`.
    pattern*: Pattern

proc embed*(p: Pattern): Value =
  EmbeddedPattern(pattern: p).embed

proc toPattern(b: sink PatternBind): Pattern =
  Pattern(orKind: PatternKind.`bind`, `bind`: b)

proc toPattern(l: sink PatternLit): Pattern =
  Pattern(orKind: PatternKind.`lit`, lit: l)

proc toPattern(a: sink AnyAtom): Pattern =
  PatternLit(value: a).toPattern

func newPattern*(kind: GroupTypeKind): Pattern =
  ## Convenience func for building group patterns.
  Pattern(orKind: PatternKind.`group`, group:
    PatternGroup(`type`: GroupType(orKind: kind)))

proc grab*(p: sink Pattern): Pattern =
  PatternBind(pattern: p).toPattern

proc drop*(): Pattern = Pattern(orKind: PatternKind.`discard`)
  ## Create a pattern to match any value without capture.

proc grab*(): Pattern = drop().grab()
  ## Create a pattern to capture any value.

proc match*(pr: Value): Pattern =
  ## Convert a `Preserve` value to a `Pattern`.
  runnableExamples:
    from std/unittest import check
    import pkg/preserves
    check:
      $("""<foo "bar" #"00" [0 1 2.0] {maybe: #t} <_>>""".parsePreserves.match) ==
        """<group <rec foo> {0: <lit "bar"> 1: <lit #"00"> 2: <group <arr> {0: <lit 0> 1: <lit 1> 2: <lit 2.0>}> 3: <group <dict> {maybe: <lit #t>}> 4: <_>}>"""

  case pr.kind
  of pkBoolean:
    result = AnyAtom(orKind: AnyAtomKind.`bool`, bool: pr.bool).toPattern
  of pkFloat:
    result = AnyAtom(orKind: AnyAtomKind.`double`, double: pr.float).toPattern
  of pkRegister:
    result = AnyAtom(orKind: AnyAtomKind.`int`, int: pr.register).toPattern
  of pkBigInt:
    result = drop()
  of pkString:
    result = AnyAtom(orKind: AnyAtomKind.`string`, string: pr.string).toPattern
  of pkByteString:
    result = AnyAtom(orKind: AnyAtomKind.`bytes`, bytes: pr.bytes).toPattern
  of pkSymbol:
    result = AnyAtom(orKind: AnyAtomKind.`symbol`, symbol: pr.symbol).toPattern

  of pkRecord:
    if pr.isRecord("_", 0): result = drop()
    elif pr.isRecord("bind", 1):
      result = pr.fields[0].match
    else:
      result = newPattern(GroupTypeKind.rec)
      result.group.`type`.rec.label = pr.label
      var i: int
      for v in pr.fields:
        result.group.entries[%i] = v.match
        inc i

  of pkSequence:
    result = newPattern(GroupTypeKind.arr)
    for i, v in pr.sequence:
      result.group.entries[%i] = v.match

  of pkSet: result = drop()
    # Cannot construct a pattern over a set literal.

  of pkDictionary:
    result = newPattern(GroupTypeKind.dict)
    for key, val in pr.pairs:
      result.group.entries[key] = val.match

  of pkEmbedded:
    let emb = pr.unembed(EmbeddedPattern)
    if emb.isSome:
      result = emb.get.pattern
    else:
      result = drop()
      # TODO: AnyAtom(orKind: AnyAtomKind.`embedded`, embedded: …).

proc match*[T](x: T): Pattern =
  ## Construct a `Pattern` that matches the given value of type `T`.
  runnableExamples:
    from std/unittest import check
    check:
      $match(true) == "<lit #t>"
      $match(3.14) == "<lit 3.14>"
      $match([0, 1, 2, 3]) == "<group <arr> {0: <lit 0> 1: <lit 1> 2: <lit 2> 3: <lit 3>}>"
  try: result = x.toPreserves.match
  except: result = drop()

proc drop*[T](x: T): Pattern {.deprecated: "use match instead of drop".} = match(x)

proc grabTypeFlat*(typ: static typedesc): Pattern =
  ## Derive a `Pattern` from type `typ`.
  ## This works for `tuple` and `object` types but in the
  ## general case will return a wildcard binding.
  runnableExamples:
    import pkg/preserves
    from std/unittest import check
    check:
      $grabTypeFlat(array[3, int]) ==
        """<group <arr> {0: <bind <_>> 1: <bind <_>> 2: <bind <_>> 3: <bind <_>>}>"""
    type
      Point = tuple[x: int; y: int]
      Rect {.preservesRecord: "rect".} = tuple[a: Point; B: Point]
      ColoredRect {.preservesDictionary.} = tuple[color: string; rect: Rect]
    check:
      $(grabTypeFlat Point) ==
        "<group <arr> {0: <bind <_>> 1: <bind <_>>}>"
      $(grabTypeFlat Rect) ==
        "<group <rec rect> {0: <group <arr> {0: <bind <_>> 1: <bind <_>>}> 1: <group <arr> {0: <bind <_>> 1: <bind <_>>}>}>"
      $(grabTypeFlat ColoredRect) ==
        "<group <dict> {color: <bind <_>> rect: <group <rec rect> {0: <group <arr> {0: <bind <_>> 1: <bind <_>>}> 1: <group <arr> {0: <bind <_>> 1: <bind <_>>}>}>}>"
  when typ is ref:
    result = grabTypeFlat(pointerBase(typ))
  elif typ.hasPreservesRecordPragma:
    result = newPattern(GroupTypeKind.rec)
    result.group.`type`.rec.label = typ.recordLabel.toSymbol
    var i {.used.}: int
    for _, f in fieldPairs(default typ):
      result.group.entries[%i] = grabTypeFlat(typeof f)
      inc i
  elif typ.hasPreservesDictionaryPragma:
    result = newPattern(GroupTypeKind.dict)
    for key, val in fieldPairs(default typ):
      result.group.entries[key.toSymbol] = grabTypeFlat(typeof val)
  elif typ is tuple:
    result = newPattern(GroupTypeKind.arr)
    var i: int
    for _, f in fieldPairs(default typ):
      result.group.entries[%i] = grabTypeFlat(typeof f)
      inc i
  elif typ is array:
    result = newPattern(GroupTypeKind.arr)
    for i in 0..len(typ):
      result.group.entries[%i] = grab()
  elif typ is Option:
    {.error: "refusing to generate a pattern for " & $typ.}
  else:
    grab()

proc matchType*(typ: static typedesc): Pattern =
  ## Derive a `Pattern` from type `typ` without any bindings.
  when typ is ref:
    result = matchType(pointerBase(typ))
  elif typ.hasPreservesRecordPragma:
    result = newPattern(GroupTypeKind.rec)
    result.group.`type`.rec.label = typ.recordLabel.toSymbol
    var i {.used.}: int
    for _, f in fieldPairs(default typ):
      var p = matchType(typeof f)
      if not p.isNil:
        result.group.entries[%i] = p
      inc i
  elif typ.hasPreservesDictionaryPragma:
    result = newPattern(GroupTypeKind.dict)
    for key, val in fieldPairs(default typ):
      var p = matchType(typeof val)
      if not p.isNil:
        result.group.entries[key.toSymbol] = p
  elif typ is tuple:
    result = newPattern(GroupTypeKind.arr)
    var i: int
    for _, f in fieldPairs(default typ):
      var p = matchType(typeof f)
      if not p.isNil:
        result.group.entries[%i] = p
      inc i
  elif typ is array:
    result = newPattern(GroupTypeKind.arr)
    let elemPat = typ.default.elementType
    for i in 0..typ.high:
      result.group.entries[%i] = elemPat
  else:
    result = drop()

proc grabType*(typ: static typedesc): Pattern =
  PatternBind(pattern: typ.matchType).toPattern

proc grabWithin*(T: static typedesc): Pattern =
  ## Construct a `Pattern` that binds the fields within type `T`.
  result = matchType(T)
  for entry in result.group.entries.mvalues:
    case entry.orKind
    of `discard`: entry = grab()
    of `bind`, `lit`: discard
    of `group`: entry = grab(entry)

proc grabWithinType*(T: static typedesc): Pattern {.
  deprecated: "use grabWithin".} = grabWithin(T)

proc bindEntries(group: var PatternGroup; bindings: openArray[(int, Pattern)]) =
  ## Set `bindings` for a `group`.
  for (i, pat) in bindings: group.entries[%i] = pat

proc match*(typ: static typedesc; bindings: sink openArray[(int, Pattern)]): Pattern =
  ## Construct a `Pattern` from type `typ` with pattern `bindings` by integer offset.
  when typ is ptr | ref:
    grab(pointerBase(typ), bindings)
  elif typ.hasPreservesRecordPragma:
    result = newPattern(GroupTypeKind.rec)
    result.group.`type`.rec.label = typ.recordLabel.toSymbol
    bindEntries(result.group, bindings)
  elif typ is tuple:
    result = newPattern(GroupTypeKind.arr)
    bindEntries(result.group, bindings)
  else:
    {.error: "grab with indexed bindings not implemented for " & $typ.}

proc grab*(typ: static typedesc; bindings: sink openArray[(int, Pattern)]): Pattern {.deprecated: "this is a match not a grab".} = match(typ, bindings)

proc match*(typ: static typedesc; bindings: sink openArray[(Value, Pattern)]): Pattern =
  ## Construct a `Pattern` from type `typ` with dictionary field `bindings`.
  when typ.hasPreservesDictionaryPragma:
    var group = PatternGroup(`type`: GroupType(orKind: GroupTypeKind.`dict`))
    for key, val in bindinds: group.entries[key] = val
    group.toPattern
  else:
    {.error: "grab with dictionary bindings not implemented for " & $typ.}

proc grabLit*(): Pattern =
  runnableExamples:
    from std/unittest import check
    check:
      $grabLit() == """<group <rec lit> {0: <bind <_>>}>"""
  grabTypeFlat(dataspacePatterns.PatternLit)

proc grabDict*(): Pattern =
  grabTypeFlat(dataspacePatterns.GroupTypeDict)

proc unpackLiterals*(pr: Value): Value =
  result = pr
  apply(result) do (pr: var Value):
    if pr.isRecord("lit", 1) or pr.isRecord("dict", 1) or pr.isRecord("arr", 1) or pr.isRecord("set", 1):
      pr = pr.record[0]

proc inject*(pattern: sink Pattern; p: Pattern; path: varargs[Value, toPreserves]
    ): Pattern {.raises: [ValueError]} =
  ## Inject `p` inside `pattern` at `path`.
  ## Injects are made at offsets indexed by the discard (`<_>`) patterns in `pat`.
  proc inject(pat: var Pattern; path: openarray[Value]) {.raises: [ValueError]} =
    if len(path) == 0:
      pat = p
    elif pat.orKind != PatternKind.`group`:
      raise newException(ValueError, "cannot inject along specified path")
    else:
      inject(pat.group.entries[path[0]], path[1..path.high])
  result = pattern
  inject(result, path)

proc matchRecord*(label: Value, fields: varargs[Pattern]): Pattern =
  runnableExamples:
    from std/unittest import check
    import pkg/preserves
    check:
      $matchRecord("Says".toSymbol, grab(), grab()) ==
        """<group <rec Says> {0: <bind <_>> 1: <bind <_>>}>"""
  result = newPattern(GroupTypeKind.`rec`)
  result.group.`type`.rec.label = label
  for i, f in fields: result.group.entries[%i] = f

proc matchRecord*(label: string; fields: varargs[Pattern]): Pattern =
  matchRecord(label.toSymbol, fields)

func matchRecord*(label: Value, fields: openarray[(int, Pattern)]): Pattern =
  runnableExamples:
    from std/unittest import check
    import pkg/preserves
    check:
      $matchRecord("Says".toSymbol, {0:grab(), 1:grab()}) ==
        """<group <rec Says> {0: <bind <_>> 1: <bind <_>>}>"""
  result = newPattern(GroupTypeKind.`rec`)
  result.group.`type`.rec.label = label
  for (n, p) in fields:
    result.group.entries[Value(kind: pkRegister, register: n)] = p

proc matchRecord*(label: string; fields: openarray[(int, Pattern)]): Pattern =
  matchRecord(label.toSymbol, fields)

proc matchTuple*(fields: varargs[Pattern, match]): Pattern =
  ## Generate a pattern that matches a bounded sequence of values (`[x, y, z]`).
  result = newPattern(GroupTypeKind.`arr`)
  for i, val in fields: result.group.entries[%i] = val

proc matchDictionary*(bindings: sink openArray[(Value, Pattern)]): Pattern =
  ## Construct a pattern that matches some dictionary pairs.
  result = newPattern(GroupTypeKind.`dict`)
  for (key, val) in bindings: result.group.entries[key] = val

proc matchDictionary*(bindings: sink openArray[(string, Pattern)]): Pattern =
  ## Construct a pattern that matches some dictionary pairs.
  ## Keys are converted from strings to symbols.
  result = newPattern(GroupTypeKind.`dict`)
  for (key, val) in bindings: result.group.entries[toSymbol key] = val

proc depattern(group: PatternGroup; values: var seq[Value]; index: var int): Value {.
    raises: [Exception].}

proc depattern(pat: Pattern; values: var seq[Value]; index: var int): Value {.
    raises: [Exception].} =
  case pat.orKind
  of PatternKind.`discard`:
    discard
  of PatternKind.`bind`:
    if index < values.len:
      result = move values[index]
      inc index
  of PatternKind.`lit`:
    result = pat.`lit`.value.toPreserves
  of PatternKind.`group`:
    result = depattern(pat.group, values, index)

proc depattern(group: PatternGroup; values: var seq[Value]; index: var int): Value {.
    raises: [Exception].} =
  case group.`type`.orKind
  of GroupTypeKind.rec:
    result = initRecord(group.`type`.rec.label, group.entries.len)
    var i: int
    for key, val in group.entries:
      if i.fromPreserves key:
        result[i] = depattern(val, values, index)
  of GroupTypeKind.arr:
    result = initSequence(group.entries.len)
    var i: int
    for key, val in group.entries:
      if i.fromPreserves key:
        result[i] = depattern(val, values, index)
  of GroupTypeKind.dict:
    result = initDictionary(group.entries.len)
    for key, val in group.entries:
      result[key] = depattern(val, values, index)

proc depattern*(pat: Pattern; values: sink seq[Value]): Value {.
    raises: [Exception].} =
  ## Convert a `Pattern` to a `Value` while replacing binds with `values`.
  runnableExamples:
    from std/unittest import check
    import pkg/preserves, pkg/preserves/sugar
    type Foo {.preservesRecord: "foo".} = object
      a, b: int
    let pat = grabTypeFlat Foo
    let val = depattern(pat, @[%1, %5])
    check $val == "<foo 1 5>"
  var index: int
  depattern(pat, values, index)

type Literal*[T] = object
  ## A wrapper type to deserialize patterns to native values.
  value*: T

proc fromPreservesHook*[T](lit: var Literal[T]; pr: Value): bool {.raises: [Exception].} =
  var pat: Pattern
  pat.fromPreserves(pr) and lit.value.fromPreserves(depattern(pat, @[]))

proc toPreservesHook*[T](lit: Literal[T]): Value =
  lit.value.match.toPreserves

when isMainModule:
  var
    lit: Literal[string]
    pr = parsePreserves """<lit "foo">"""
  assert fromPreservesHook(lit, pr)
  assert lit.value == "foo"
  pr = lit.toPreservesHook

func isGroup(pat: Pattern): bool =
  pat.orKind == PatternKind.`group`

func isMetaDict(pat: Pattern): bool =
  pat.orKind == PatternKind.`group` and
    pat.group.type.orKind == GroupTypeKind.dict

proc metaApply(result: var Pattern; pat: Pattern; path: openarray[Value], offset: int) {.raises: [KeyError, ValueError].} =
  if offset == path.len:
    result = pat
  elif result.isGroup and result.group.entries[%1].isMetaDict:
    if offset == path.high:
      result.group.entries[%1].group.entries[path[offset]] = pat
    else:
      metaApply(result.group.entries[%1].group.entries[path[offset]], pat, path, succ offset)
  else:
    raise newException(ValueError, "cannot inject into non-group pattern")

proc observePattern*(pat: Pattern): Pattern =
  ## Construct a pattern that matches an  `Observe` assertion for `pat`.
  runnableExamples:
    import pkg/preserves
    let
      sample = parsePreserves"<sample>"
      observation = sample.match.observePattern
    assert $observation ==
      "<group <rec Observe> {0: <group <rec group> {0: <group <rec rec> {0: <lit sample>}> 1: <group <dict> {}>}> 1: <_>}>"
  result = matchType Observe
  result.group.entries[%0] = pat.toPreserves.match

proc observePattern*(pat: Pattern; injects: openarray[(seq[Value], Pattern)]): Pattern {.raises: [Exception].} =
  ## Construct a pattern that matches an  `Observe` assertion for `pat`
  ## and inject patterns along paths corresponding to the source pattern `pat`.
  runnableExamples:
    import pkg/preserves, pkg/preserves/sugar
    let
      v = parsePreserves"[ {} {a: #f b: #t } ]"
      observation = v.match.observePattern { @[%1, "b".toSymbol]: grab() }
    assert $observation ==
      "<group <rec Observe> {0: <group <rec group> {0: <group <rec arr> {}> 1: <group <dict> {0: <group <rec group> {0: <group <rec dict> {}> 1: <group <dict> {}>}> 1: <group <rec group> {0: <group <rec dict> {}> 1: <group <dict> {a: <group <rec lit> {0: <lit #f>}> b: <bind <_>>}>}>}>}> 1: <_>}>"
  result = matchType Observe
  var meta = pat.toPreserves.match
  for (path, pat) in injects:
    metaApply(meta, pat, path, 0)
  result.group.entries[%0] = meta

proc observePattern*(pat: Pattern; injects: openarray[(int, Pattern)]): Pattern {.raises: [Exception].} =
  ## Sugar for observing patterns using record field offsets.
  pat.observePattern injects.map do (pair: (int, Pattern)) -> (seq[Value], Pattern):
    (@[pair[0].toPreserves], pair[1])

when isMainModule:
  stdout.writeLine stdin.readAll.parsePreserves.match
