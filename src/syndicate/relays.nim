# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[sets, tables],
  pkg/preserves,
  ./protocols/[gatekeeper, protocol, rpc, sturdy, transportAddress],
  ../syndicate, ./membranes, ./resolvers, ./rpc

when defined(posix):
  import ./sturdyrefs
  from std/os import getEnv, `/`

when defined(traceSyndicate):
  type TraceLogProc* = proc (s: string)
  var traceLogImpl*: TraceLogProc
  template trace(s: string) =
    if not traceLogImpl.isNil:
      traceLogImpl(s)
  when defined(posix):
    proc stderrLog(s: string) = stderr.writeLine(s)
    traceLogImpl = stderrLog
  else:
    proc echoLog(s: string) = echo.writeLine(s)
    traceLogImpl = echoLog
else:
  template trace(s: string) = discard

export `$`

export Route, Stdio, Tcp, WebSocket, Unix

type
  Event = protocol.Event
  Handle = actors.Handle
  Oid = sturdy.Oid
  Turn = syndicate.Turn
  WireRef = sturdy.WireRef

  PacketWriter = proc (turn: Turn; buf: seq[byte]) {.closure.}
  RelaySetup = proc (turn: Turn; relay: Relay) {.closure.}

  Relay* = ref object
    facet: Facet
    inboundAssertions: Table[Handle,
      tuple[localHandle: Handle, imported: seq[WireSymbol]]]
    outboundAssertions: Table[Handle, seq[WireSymbol]]
    pendingTurn: protocol.Turn
    exported: Membrane
    imported: Membrane
    nextLocalOid: Oid
    wireBuf: BufferedDecoder
    packetWriter: PacketWriter
    peer: Cap

  SyncPeerEntity {.final.} = ref object of EntityObj
    handleMap: Table[Handle, Handle]
    wireSym: WireSymbol
    relay: Relay
    peer: Cap

  RelayEntity {.final.} = ref object of EntityObj
    ## https://synit.org/book/protocol.html#relay-entities
    label: string
    relay: Relay

proc releaseCapOut(r: Relay; e: WireSymbol) =
  r.exported.drop e

proc syncPeerEntityPublish(turn: Turn; e: Entity; v: Value; h: Handle) =
  let e = SyncPeerEntity(e)
  e.handleMap[h] = publish(turn, e.peer, v)

proc syncPeerEntityRetract(turn: Turn; e: Entity; h: Handle) =
  let e = SyncPeerEntity(e)
  var other: Handle
  if e.handleMap.pop(h, other):
    retract(turn, other)

proc syncPeerEntityMessage(turn: Turn; e: Entity; v: Value) =
  let e = SyncPeerEntity(e)
  if not e.wireSym.isNil:
    e.relay.releaseCapOut(e.wireSym)
  message(turn, e.peer, v)

proc syncPeerEntitySync(turn: Turn; e: Entity; other: Cap) =
  let e = SyncPeerEntity(e)
  sync(turn, e.peer, other)

proc newSyncPeerEntity(relay: Relay; peer: Cap): SyncPeerEntity =
  SyncPeerEntity(
      facet: relay.facet,
      a: syncPeerEntityPublish,
      m: syncPeerEntityMessage,
      r: syncPeerEntityRetract,
      s: syncPeerEntitySync,
      relay: relay,
      peer: peer,
    )

proc rewriteCapOut(relay: Relay; cap: Cap; exported: var seq[WireSymbol]): WireRef =
  if cap.target of RelayEntity and cap.target.RelayEntity.relay == relay and cap.caveats.len == 0:
    result = WireRef(orKind: WireRefKind.yours, yours: WireRefYours(oid: cap.target.oid))
  else:
    var ws = grab(relay.exported, cap)
    if ws.isNil:
      ws = newWireSymbol(relay.exported, relay.nextLocalOid, cap)
      inc relay.nextLocalOid
    exported.add ws
    result = WireRef(
        orKind: WireRefKind.mine,
        mine: WireRefMine(oid: ws.oid))

proc rewriteOut(relay: Relay; v: Value):
    tuple[rewritten: Value, exported: seq[WireSymbol]] =
  var exported: seq[WireSymbol]
  result.rewritten = mapEmbeds(v) do (pr: Value) -> Value:
    let o = pr.unembed(Cap); if o.isSome:
      result = rewriteCapOut(relay, o.get, exported).toPreserves
      result.embedded = true
    else: result = pr
  result.exported = exported

proc register(relay: Relay; v: Value; h: Handle): tuple[rewritten: Value, exported: seq[WireSymbol]] =
  result = rewriteOut(relay, v)
  relay.outboundAssertions[h] = result.exported

proc deregister(relay: Relay; h: Handle) =
  var outbound: seq[WireSymbol]
  if relay.outboundAssertions.pop(h, outbound):
    for e in outbound: releaseCapOut(relay, e)

proc send(relay: Relay; turn: Turn; rOid: protocol.Oid; m: Event) =
  if relay.pendingTurn.len == 0:
    queueTurn(turn.facet, externalCause "send Syndicate packet") do (turn: Turn):
      var pkt = Packet(
        orKind: PacketKind.Turn,
        turn: move relay.pendingTurn)
      trace("C: " & $pkt)
      relay.packetWriter(turn, encode pkt)
  relay.pendingTurn.add TurnEvent(oid: rOid, event: m)

proc send(re: RelayEntity; turn: Turn; ev: Event) =
  send(re.relay, turn, protocol.Oid re.oid, ev)

proc relayPublish(turn: Turn; e: Entity; v: Value; h: Handle) =
  let re = RelayEntity(e)
  re.send(turn, Event(
      orKind: EventKind.Assert,
      `assert`: protocol.Assert(
        assertion: re.relay.register(v, h).rewritten,
        handle: h)))

proc relayRetract(turn: Turn; e: Entity; h: Handle) =
  let re = RelayEntity(e)
  re.relay.deregister h
  re.send(turn, Event(
    orKind: EventKind.Retract,
    retract: Retract(handle: h)))

proc relayMessage(turn: Turn; e: Entity; v: Value) =
  let re = RelayEntity(e)
  var (value, exported) = rewriteOut(re.relay, v)
  assert(len(exported) == 0, "cannot send a reference in a message")
  if len(exported) == 0:
    re.send(turn, Event(orKind: EventKind.Message, message: Message(body: value)))

proc relaySync(turn: Turn; e: Entity; peer: Cap) =
  let re = RelayEntity(e)
  var
    peerEntity = newSyncPeerEntity(re.relay, peer)
    exported: seq[WireSymbol]
    wr = rewriteCapOut(re.relay, peerEntity.newCap(), exported)
  peerEntity.wireSym = exported[0]
  var ev = Event(orKind: EventKind.Sync)
  ev.sync.peer = wr.toPreserves.embed
  re.send(turn, ev)

proc newRelayEntity(facet: Facet, label: string; r: Relay; o: Oid): RelayEntity =
  RelayEntity(
      facet: facet,
      a: relayPublish,
      r: relayRetract,
      m: relayMessage,
      s: relaySync,
      label: label, relay: r, oid: o,
    )

using
  relay: Relay
  facet: Facet

proc lookupLocal(relay; oid: Oid): Cap =
  let sym = relay.exported.grab oid
  if not sym.isNil:
    result = sym.cap

proc rewriteCapIn(relay; facet; n: WireRef, imported: var seq[WireSymbol]): Cap =
  case n.orKind
  of WireRefKind.mine:
    var e = relay.imported.grab(n.mine.oid)
    if e.isNil:
      e = newWireSymbol(
          relay.imported,
          n.mine.oid,
          newRelayEntity(facet, "rewriteCapIn", relay, n.mine.oid).newCap(),
        )
    imported.add e
    result = e.cap
  of WireRefKind.yours:
    result = relay.lookupLocal(n.yours.oid)
    if result.isNil:
      result = newInertCap()
    elif n.yours.attenuation.len > 0:
      result = attenuate(result, n.yours.attenuation)

proc rewriteIn(relay; facet; v: Value):
    tuple[rewritten: Value; imported: seq[WireSymbol]] =
  var imported: seq[WireSymbol]
  result.rewritten = mapEmbeds(v) do (pr: Value) -> Value:
    let wr = pr.preservesTo WireRef; if wr.isSome:
      result = rewriteCapIn(relay, facet, wr.get, imported).embed
    else:
      result = pr
  result.imported = imported

proc close(r: Relay) = discard

proc dispatch(relay: Relay; turn: Turn; cap: Cap; event: Event) =
  case event.orKind
  of EventKind.Assert:
    let (a, imported) = rewriteIn(relay, turn.facet, event.assert.assertion)
    relay.inboundAssertions[event.assert.handle] = (publish(turn, cap, a), imported,)

  of EventKind.Retract:
    let remoteHandle = event.retract.handle
    var outbound: tuple[localHandle: Handle, imported: seq[WireSymbol]]
    if relay.inboundAssertions.pop(remoteHandle, outbound):
      for e in outbound.imported: relay.imported.drop e
      turn.retract(outbound.localHandle)

  of EventKind.Message:
    let (a, imported) = rewriteIn(relay, turn.facet, event.message.body)
    assert imported.len == 0, "Cannot receive transient reference"
    turn.message(cap, a)

  of EventKind.Sync:
    turn.sync(cap) do (turn: Turn):
      var
        (v, imported) = rewriteIn(relay, turn.facet, event.sync.peer)
        peer = unembed(v, Cap)
      if peer.isSome:
        turn.message(get peer, true)
      for e in imported: relay.imported.drop e

proc dispatch(relay: Relay; v: Value) =
  trace("S: " & $v)
  queueTurn(relay.facet, externalCause "recv Syndicate packet") do (t: Turn):
    var pkt: Packet
    if pkt.fromPreserves(v):
      case pkt.orKind
      of PacketKind.Turn:
        # https://synit.org/book/protocol.html#turn-packets
        for te in pkt.turn:
          let r = lookupLocal(relay, te.oid.Oid)
          if not r.isNil:
            dispatch(relay, t, r, te.event)
      of PacketKind.Error:
        # https://synit.org/book/protocol.html#error-packets
        when defined(posix):
          stderr.writeLine("Error from server: ", pkt.error.message, " (detail: ", pkt.error.detail, ")")
        close relay
      of PacketKind.Extension:
        # https://synit.org/book/protocol.html#extension-packets
        discard
      of PacketKind.Nop:
        discard
    else:
      when defined(posix):
        stderr.writeLine("discarding undecoded packet ", v)

proc recv(relay: Relay; buf: openarray[byte]; slice: Slice[int]) =
  feed(relay.wireBuf, buf, slice)
  var pr = relay.wireBuf.decode()
  while pr.isSome:
    relay.dispatch(get pr)
    pr = relay.wireBuf.decode()

type
  RelayOptions* = object of RootObj
    packetWriter*: PacketWriter

  RelayActorOptions* = object of RelayOptions
    initialOid*: Option[Oid]
    initialCap*: Cap
    nextLocalOid*: Option[Oid]

proc spawnRelay(name: string; turn: Turn; opts: RelayActorOptions; setup: RelaySetup) =
  linkActor(turn, name) do (turn: Turn):
    turn.facet.preventInertCheck()
    let relay = Relay(
        facet: turn.facet,
        packetWriter: opts.packetWriter,
        wireBuf: newBufferedDecoder(0),
      )
    if not opts.initialCap.isNil:
      var exported: seq[WireSymbol]
      discard rewriteCapOut(relay, opts.initialCap, exported)
    opts.nextLocalOid.map do (oid: Oid):
      relay.nextLocalOid =
        if oid == 0.Oid: 1.Oid
        else: oid
    assert opts.initialOid.isSome
    if opts.initialOid.isSome:
      var
        imported: seq[WireSymbol]
        wr = WireRef(
          orKind: WireRefKind.mine,
          mine: WireRefMine(oid: opts.initialOid.get))
      relay.peer = rewriteCapIn(relay, turn.facet, wr, imported)
      assert not relay.peer.isNil
    setup(turn, relay)

when defined(posix):

  import std/[oserrors, posix]
  import pkg/sys/[files, handles, ioqueue, sockets]
  export transportAddress.Unix

  const
    STDIN = 0
    STDOUT = 1
    STDERR = 2

  type StdioEntity {.final.} = ref object of EntityObj
    relay: Relay
    files: array[2, AsyncFile]
    alive: bool

  proc handleControlMessage(turn: Turn; e: Entity; v: Value) =
    if v.preservesTo(ForceDisconnect).isSome:
      StdioEntity(e).alive = false

  proc loop(entity: StdioEntity) {.asyncio.} =
    let buf = new seq[byte]
    entity.alive = true
    while entity.alive:
      buf[].setLen(0x1000)
      let n = entity.files[STDIN].read(buf)
      if n > 0:
        entity.relay.recv(buf[], 0..<n)
      else:
        entity.alive = false
        if n < 0:
          queueTurn(entity.facet, externalCause osLastError().osErrorMsg, stopActor)
    queueTurn(entity.facet, externalCause "stdio closed", stopActor)

  when not defined(plan9):
    from std/terminal import isatty

  proc connectStdioTransport(turn: Turn; answer: AnswerReceiver) =
    ## Connect to an external dataspace over stdio.
    when not defined(plan9): # Plan9 does not support teletypewriters.
      doAssert not(stdin.isatty or stdout.isatty):
        "Refusing to use a TTY as a Syndicate protocol transport. $SYNDICATE_ROUTE must be set."
    let
      facet = turn.facet
      localDataspace = newDataspace(facet)
      entity = StdioEntity(facet: facet, m: handleControlMessage)

    proc stdoutWriter(turn: Turn; buf: seq[byte]) =
      if entity.files[STDOUT].fd != InvalidFD:
        if entity.files[STDOUT].write(buf) != buf.len:
          stopActor(turn, "short write to stdout")
    var opts = RelayActorOptions(
        packetWriter: stdoutWriter,
        initialCap: localDataspace,
        initialOid: 0.Oid.some,
      )
    spawnRelay("stdio", turn, opts) do (turn: Turn; relay: Relay):
      entity.facet = turn.facet
      entity.relay = relay
      for i, _ in entity.files:
        let
          fd = fcntl(cint i, F_DUPFD, STDERR)
          flags =  fcntl(fd, F_GETFL, 0)
        if flags < 0: raiseOSError(osLastError())
        if close(cint i) < 0 or dup2(STDERR, cint i) < 0 or
            fcntl(fd, F_SETFL, flags or O_NONBLOCK) < 0:
          raiseOSError(osLastError())
          # close STDIN, STDOUT and replace with STDERR
        entity.files[i] = newAsyncFile(FD fd)
      entity.facet.onStop do (turn: Turn):
        entity.alive = false
        for f in entity.files:
          close(f)
          # Close files to remove them from the ioqueue
      discard trampoline:
        whelp loop(entity)
      ok(turn, answer, ConnectedTransport(
          `addr`: Stdio().toPreserves,
          control: entity.newCap(),
          responderSession: localDataspace,
        ).toPreserves)

  type
    TcpEntity {.final.} = ref object of EntityObj
      relay: Relay
      sock: AsyncConn[sockets.Protocol.TCP]
      alive: bool

    UnixEntity {.final.} = ref object of EntityObj
      relay: Relay
      sock: AsyncConn[sockets.Protocol.Unix]
      alive: bool

  proc forceDisconnectTcp(turn: Turn; e: Entity; v: Value) =
    if v.preservesTo(ForceDisconnect).isSome:
      TcpEntity(e).alive = false

  proc forceDisconnectUnix(turn: Turn; e: Entity; v: Value) =
    if v.preservesTo(ForceDisconnect).isSome:
      UnixEntity(e).alive = false

  template bootSocketEntity() {.dirty.} =
    proc setup(turn: Turn) {.closure.} =
      proc kill(turn: Turn) =
        if entity.alive:
          entity.alive = false
          close(entity.sock)
      entity.facet.onStop(kill)
      ok(turn, answer, ConnectedTransport(
          `addr`: ta.toPreserves,
          control: entity.newCap(),
          responderSession: entity.relay.peer,
        ).toPreserves)
    queueTurn(entity.relay.facet, externalCause "connect socket", setup)
    let buf = new seq[byte]
    entity.alive = true
    while entity.alive:
      buf[].setLen(0x1000)
      let n = read(entity.sock, buf)
      if n > 0:
        entity.relay.recv(buf[], 0..<n)
      else:
        entity.alive = false
        if n < 0:
          queueTurn(entity.facet, externalCause osLastError().osErrorMsg, stopActor)
    queueTurn(entity.facet, externalCause "stdio closed", stopActor)
    # the socket closes when the actor is stopped

  proc boot(entity: TcpEntity; answer: AnswerReceiver; ta: transportAddress.Tcp) {.asyncio.} =
    entity.sock = connectTcpAsync(ta.host, Port ta.port)
    bootSocketEntity()

  proc boot(entity: UnixEntity; answer: AnswerReceiver; ta: transportAddress.Unix) {.asyncio.} =
    entity.sock = connectUnixAsync(ta.path)
    bootSocketEntity()

  template spawnSocketRelay() {.dirty.} =
    proc writeConn(turn: Turn; buf: seq[byte]) =
      if entity.alive:
        discard trampoline:
          whelp write(entity.sock, buf)
    var ops = RelayActorOptions(
        packetWriter: writeConn,
        initialOid: 0.Oid.some,
      )
    spawnRelay("socket", turn, ops) do (turn: Turn; relay: Relay):
      entity.facet = turn.facet
      entity.relay = relay
      discard trampoline:
        whelp boot(entity, answer, ta)

  proc connectTransport(turn: Turn; answer: AnswerReceiver; ta: transportAddress.Tcp) =
    let entity =  TcpEntity(facet: turn.facet, m: forceDisconnectTcp)
    spawnSocketRelay()

  proc connectTransport(turn: Turn; answer: AnswerReceiver; ta: transportAddress.Unix) =
    let entity =  UnixEntity(facet: turn.facet, m: forceDisconnectUnix)
    spawnSocketRelay()

  proc spawnPosixResolvers*(turn: Turn; ds: Cap) =

    onQuestion(turn, ds, ConnectTransport?:{0: transportAddress.Stdio.matchType}) do (
        turn: Turn; answer: AnswerReceiver; _: seq[Value]):
      tryAnswer(turn, answer):
        connectStdioTransport(turn, answer)

    onQuestion(turn, ds, ConnectTransport?:{0: transportAddress.Unix.grabType}) do (
        turn: Turn; answer: AnswerReceiver; captures: seq[Value]):
      tryAnswer(turn, answer):
        var ta: transportAddress.Unix
        doAssert ta.fromPreserves(captures[0])
        connectTransport(turn, answer, ta)

elif defined(solo5):
  import
    std/solo5,
    pkg/taps

  type
    TcpEntity {.final.} = ref object of EntityObj
      relay: Relay
      conn: Connection
      decoder: BufferedDecoder

  proc forceDisconnect(turn: Turn; e: Entity; v: Value) =
    if v.preservesTo(ForceDisconnect).isSome:
      TcpEntity(e).conn.abort()

  proc connectTransport(turn: Turn; answer: AnswerReceiver; ta: transportAddress.Tcp) =
    let entity = TcpEntity(facet: turn.facet, m: forceDisconnect)

    proc writeConn(turn: Turn; buf: seq[byte]) =
      assert not entity.conn.isNil
      entity.conn.batch:
        entity.conn.send(buf)
    var ops = RelayActorOptions(
        packetWriter: writeConn,
        initialOid: 0.Oid.some,
      )
    spawnRelay("tcp", turn, ops) do (turn: Turn; relay: Relay):
      entity.facet = turn.facet
      entity.relay = relay

      var ep = newRemoteEndpoint()
      if ta.host.isIpAddress:
        ep.with ta.host.parseIpAddress
      else:
        ep.withHostname ta.host
      ep.with ta.port.Port

      var tp = newTransportProperties()
      tp.require "reliability"
      tp.ignore "congestion-control"
      tp.ignore "preserve-order"

      var preconn = newPreconnection(
        remote=[ep], transport=tp.some)
      entity.conn = preconn.initiate()
      entity.facet.onStop do (turn: Turn):
        entity.conn.close()
      entity.conn.onConnectionError do (err: ref Exception):
        entity.facet.actor.fail("connection error", err)
      entity.conn.onClosed():
        queueTurn(entity.facet, externalCause "connection closed", stopFacet)
      entity.conn.onReceivedPartial do (data: seq[byte]; ctx: MessageContext; eom: bool):
        entity.relay.recv(data, data.low..data.high)
        if eom:
          queueTurn(entity.facet, externalCause "connection shutdown", stopFacet)
        else:
          entity.conn.receive()
      entity.conn.onReady do ():
        queueTurn(entity.facet, externalCause "connection ready") do (turn: Turn):
          ok(turn, answer, ConnectedTransport(
              `addr`: ta.toPreserves,
              control: entity.newCap(),
              responderSession: entity.relay.peer,
            ).toPreserves)
          entity.conn.receive()

proc spawnTcpResolver*(turn: Turn; ds: Cap) =
  onQuestion(turn, ds, ConnectTransport?:{0: transportAddress.Tcp.grabType}) do (
        turn: Turn; answer: AnswerReceiver; captures: seq[Value]):
    tryAnswer(turn, answer):
      var ta: transportAddress.Tcp
      doAssert ta.fromPreserves(captures[0])
      connectTransport(turn, answer, ta)

proc spawnRefResolver*(turn: Turn; ds: Cap) =
  let pat = ResolvePathStep.match({ 0: grab(), 1: grab() })
  onQuestion(turn, ds, pat) do (
      turn: Turn; answer: AnswerReceiver; captures: seq[Value]):
    tryAnswer(turn, answer):
      var origin: Cap
      doAssert origin.fromPreserves(captures[0])
      publish(turn, origin, Resolve(
          step: captures[1],
          observer: newResolverReceiver(turn.facet, answer),
        ))

proc spawnRelays*(turn: Turn; ds: Cap) =
  ## Spawn actors that manage routes and appease gatekeepers.
  when defined(posix) and not defined(nimdoc):
    spawnPosixResolvers(turn, ds)
  spawnTcpResolver(turn, ds)
  spawnRefResolver(turn, ds)
  onQuestion(turn, ds, ResolvePath.grabWithin) do (
      turn: Turn; answer: AnswerReceiver; captures: seq[Value]):
    var route: Route
    doAssert route.fromPreserves(captures[0])
    resolve(turn, ds, answer, route)

type
  BootProc* = proc (turn: Turn; ds: Cap) {.closure.}

proc resolve*(turn: Turn; ds: Cap; route: Route; bootProc: BootProc) =
  ## Resolve `route` within `ds` and call `bootProc` with resolved capabilities.
  ## If a resolved route is retracted then `bootProc` will be called again with
  ## the next available route.
  onAnswer(turn, ds, ResolvePath(route: route)) do (turn: Turn; resp: Value):
    var res: Result
    if res.fromPreserves(resp):
      let rp = res.getOk(ResolvedPath)
      bootProc(turn, rp.responderSession.Cap)
        # TODO: pass on the address and the control cap.

proc resolve*(turn: Turn; route: Route; bootProc: BootProc) =
  ## Resolve `route` and call `bootProc` with resolved capability.
  let ds = turn.facet.newDataspace()
  resolve(turn, ds, route, bootProc)
  linkActor(turn, "relay-resolver") do (turn: Turn):
    spawnRelays(turn, ds)

when defined(solo5):
    proc envRoute*: Route =
      var pr = parsePreserves $solo5_start_info.cmdline
      if result.fromPreserves pr:
        return
      elif pr.isSequence:
        for e in pr:
          if result.fromPreserves e:
            return
      quit("failed to parse command line for route to Syndicate gatekeeper")

elif defined(posix):
  const defaultRoute* = "<route [<stdio>]>"

  proc envRoute*: Route =
    ## Get an route to a Syndicate capability from the calling environment.
    ## On UNIX this is the SYNDICATE_ROUTE environmental variable with a
    ## fallack to a defaultRoute_.
    ## See https://git.syndicate-lang.org/syndicate-lang/syndicate-protocols/raw/branch/main/schemas/gatekeeper.prs.
    var text = getEnv("SYNDICATE_ROUTE", defaultRoute)
    try:
      if result.fromPreserves text.parsePreserves: return
    except ValueError: discard
    raise newException(ValueError, "failed to parse $SYNDICATE_ROUTE " & $text)

proc resolveEnvironment*(turn: Turn; bootProc: BootProc) =
  ## Resolve a capability from the calling environment
  ## and call `bootProc`. See envRoute_.
  resolve(turn, envRoute(), bootProc)

# TODO: define a runActor that comes preloaded with relaying
