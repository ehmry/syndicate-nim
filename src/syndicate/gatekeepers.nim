# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  pkg/preserves,
  ../syndicate, ./protocols/[gatekeeper, rpc]

export gatekeeper, rpc

proc resultOk*[T](v: T): Result =
  ## Sugar for producing a `ok` record.
  result = Result(orKind: ResultKind.ok)
  result.ok.value = v.toPreserves

proc resultError*[T](v: T): Result =
  ## Sugar for producing a `ok` record.
  result = Result(orKind: ResultKind.error)
  result.error.error = v.toPreserves

proc toResolved*(res: Result): Resolved =
  case res.orKind
  of ResultKind.ok:
    var cap = res.ok.value.unembed(Cap)
    result = Resolved(orKind: ResolvedKind.accepted)
    result.accepted.responderSession = cap.get
  of ResultKind.error:
    result = Resolved(orKind: ResolvedKind.Rejected)
    result.rejected.detail = res.error.error

proc serveGatekeeper*[T](turn: Turn; ds: Cap; resolve: proc (turn: Turn; step: T): Result): Facet {.discardable.} =
  ## Call `resolve` for resolve requests at `gk` that match type `T`.
  turn.withNewFacet:
    result = turn.facet
    during(turn, ds, Resolve?:{ 0: grabType(T), 1: grab() }) do (step: T, obs: Cap):
      try:
        # discard spawnActor(turn, "serve") do (turn: Turn):
        block:
          var r = resolve(turn, step)
          publish(turn, obs, r)
          publish(turn, obs, r.toResolved)
      except CatchableError as err:
        var reject = Resolved(orKind: ResolvedKind.Rejected)
        reject.rejected.detail = err.msg.toPreserves
        publish(turn, obs, reject)
        publish(turn, obs, err.msg.resultError)

type ResolveAction* = proc (turn: Turn; res: Resolved) {.closure.}

proc newResolveReceiver*(turn: Turn; act: ResolveAction): Cap =
  assert not act.isNil
  newDuringEntity(turn.facet, act)
