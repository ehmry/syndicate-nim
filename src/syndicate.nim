# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

## This module implements the `Syndicate DSL <https://syndicate-lang.org/doc/syndicate/>`_.

import std/[macros, tables, typetraits]

import pkg/preserves
export fromPreserves, toPreserves

import ./syndicate/[actors, analysis, dataspaces, durings, patterns]
import ./syndicate/protocols/dataspace

export actors, dataspace, dataspaces, durings, patterns, preserves

type Assertion* {.deprecated: "Assertion and Preserve[void] replaced by Value".} = Value

proc `!`*(typ: static typedesc): Pattern {.inline.} =
  patterns.matchType(typ)

proc `?`*[T](val: T): Pattern {.inline.} =
  patterns.match[T](val)

proc `?:`*(typ: static typedesc): Pattern {.inline.} =
  patterns.grabTypeFlat(typ)

proc `?:`*(typ: static typedesc; bindings: sink openArray[(int, Pattern)]): Pattern {.inline.} =
  patterns.match(typ, bindings)

proc `?:`*(typ: static typedesc; bindings: sink openArray[(Value, Pattern)]): Pattern {.inline.} =
  patterns.match(typ, bindings)

proc argumentCount(handler: NimNode): int =
  handler.expectKind {nnkDo, nnkStmtList}
  if handler.kind == nnkDo: result = pred handler[3].len

type HandlerNodes = tuple
  valuesSym, varSection, body: NimNode

proc generateHandlerNodes(handler: NimNode): HandlerNodes =
  handler.expectKind {nnkStmtList, nnkDo}
  result.valuesSym = genSym(nskVar, "values")
  let valuesTuple = newNimNode(nnkTupleTy, handler)
  case handler.kind
  of nnkStmtList:
    result.body = handler
  of nnkDo:
    let
      innerTuple = newNimNode(nnkVarTuple, handler)
      varSectionInner = newNimNode(nnkVarSection, handler).add(innerTuple)
    for i, arg in handler[3]:
      if i > 0:
        arg.expectKind nnkIdentDefs
        if arg[1].kind == nnkEmpty:
          error("type required for capture", arg)
        var def = newNimNode(nnkIdentDefs, arg)
        arg.copyChildrenTo def
        valuesTuple.add(def)
        innerTuple.add(arg[0])
    innerTuple.add(newEmptyNode(), result.valuesSym)
    result.body = newStmtList(varSectionInner, handler[6])
  else:
    discard # caught earlier by expectKind
  result.varSection = newNimNode(nnkVarSection, handler).
      add(newIdentDefs(result.valuesSym, valuesTuple))

proc wrapPublishHandler(turn, handler: NimNode): NimNode =
  var
    (valuesSym, varSection, publishBody) =
      generateHandlerNodes(handler)
    handleSym = ident"handle"
    handlerSym = genSym(nskProc, "publish")
    bindingsSym = ident"bindings"
  quote do:
    proc `handlerSym`(`turn`: Turn; `bindingsSym`: Value; `handleSym`: Handle) =
      `varSection`
      if fromPreserves(`valuesSym`, bindings):
        `publishBody`

proc wrapMessageHandler(turn, handler: NimNode): NimNode =
  var
    (valuesSym, varSection, body) =
      generateHandlerNodes(handler)
    handlerSym = genSym(nskProc, "message")
    bindingsSym = ident"bindings"
  quote do:
    proc `handlerSym`(`turn`: Turn; `bindingsSym`: Value) =
      `varSection`
      if fromPreserves(`valuesSym`, bindings):
        `body`

proc wrapDuringHandler(turn, entryBody, exitBody: NimNode): NimNode =
  var
    (valuesSym, varSection, publishBody) =
      generateHandlerNodes(entryBody)
    bindingsSym = ident"bindings"
    duringSym = genSym(nskProc, "during")
  if exitBody.isNil:
    quote do:
      proc `duringSym`(`turn`: Turn; `bindingsSym`: Value) =
        `varSection`
        if fromPreserves(`valuesSym`, `bindingsSym`):
          `publishBody`
  else:
    quote do:
      proc `duringSym`(`turn`: Turn; `bindingsSym`: Value) =
        `varSection`
        if fromPreserves(`valuesSym`, `bindingsSym`):
          `publishBody`
          onStop(facet(`turn`)) do (`turn`: Turn):
            `exitBody`

macro onPublish*(turn: untyped; ds: Cap; pattern: Pattern; handler: untyped) =
  ## Call `handler` when an assertion matching `pattern` is published at `ds`.
  let
    argCount = argumentCount(handler)
    handlerProc = wrapPublishHandler(turn, handler)
    handlerSym = handlerProc[0]
  result = quote do:
    if `argCount` != 0 and `pattern`.analyse.capturePaths.len != `argCount`:
      raiseAssert($`pattern`.analyse.capturePaths.len & " values captured but handler has " & $`argCount` & " arguments - " & $`pattern`)
    `handlerProc`
    discard observe(`turn`, `ds`, `pattern`, newPublishHandler(facet(`turn`), `handlerSym`))

macro onMessage*(turn: untyped; ds: Cap; pattern: Pattern; handler: untyped) =
  ## Call `handler` when an message matching `pattern` is broadcasted at `ds`.
  let
    argCount = argumentCount(handler)
    handlerProc = wrapMessageHandler(turn, handler)
    handlerSym = handlerProc[0]
  result = quote do:
    if `argCount` != 0 and `pattern`.analyse.capturePaths.len != `argCount`:
      raiseAssert($`pattern`.analyse.capturePaths.len & " values captured but handler has " & $`argCount` & " arguments - " & $`pattern`)
    `handlerProc`
    discard observe(`turn`, `ds`, `pattern`, newMessageHandler(facet(`turn`), `handlerSym`))

macro during*(turn: untyped; ds: Cap; pattern: Pattern; publishBody, retractBody: untyped) =
  ## Call `publishBody` when an assertion matching `pattern` is published to `ds` and
  ## call `retractBody` on retraction. Assertions that match `pattern` but are not
  ## convertable to the arguments of `publishBody` are silently discarded.
  let
    argCount = argumentCount(publishBody)
    callbackProc = wrapDuringHandler(turn, publishBody, retractBody)
    callbackSym = callbackProc[0]
  result = quote do:
    if `argCount` != 0 and `pattern`.analyse.capturePaths.len != `argCount`:
      raiseAssert($`pattern`.analyse.capturePaths.len & " values captured but handler has " & $`argCount` & " arguments - " & $`pattern`)
    `callbackProc`
    discard observe(`turn`, `ds`, `pattern`, newDuringEntity[Value](facet(`turn`), `callbackSym`))

macro during*(turn: untyped; ds: Cap; pattern: Pattern; publishBody: untyped) =
  ## Variant of `during` without a retract body.
  let
    `argCount` = argumentCount(publishBody)
    callbackProc = wrapDuringHandler(turn, publishBody, nil)
    callbackSym = callbackProc[0]
  result = quote do:
    if `argCount` != 0 and `pattern`.analyse.capturePaths.len != `argCount`:
      raiseAssert($`pattern`.analyse.capturePaths.len & " values captured but handler has " & $`argCount` & " arguments - " & $`pattern`)
    `callbackProc`
    discard observe(`turn`, `ds`, `pattern`, newDuringEntity[Value](facet(`turn`), `callbackSym`))

when defined(solo5):
  echo """
    ______
   /    \_\
  /  ,__/  \                          ____           __
 /\__/  \,  \   _______  ______  ____/ /_/________  / /____
 \/  \__/   /  / ___/ / / / __ \/ __  / / ___/ __ \/ __/ _ \
  \  '  \__/  _\_ \/ /_/ / / / / /_/ / / /__/ /_/ / /_/  __/
   \____/_/  /____/\__, /_/ /_/\____/_/\___/\__/_/\__/\___/
                 /____/
"""
