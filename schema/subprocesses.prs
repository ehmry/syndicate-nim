version 1 .
embeddedType EntityRef.Cap .

# Gatekeeper step to start a subprocess.
# The resolved subprocess capability accepts
# messages.
# Signal messages cause signals to be sent to the subprocess.
# Byte-string messages are sent to stdin.
SubprocessAdapterStep = <unix-subprocess @detail SubprocessAdapterDetail> .
SubprocessAdapterDetail =
& { argv: CommandLine, stdout: #:any }
& @env				ProcessEnv
& @dir				ProcessDir
& @clearEnv			ClearEnv
& @stderr			StderrCap
& @stdinProtocol	StdinProtocol
& @stdoutProtocol	StdoutProtocol
.

ProcessEnv = @present { env: { EnvVariable: EnvValue ...:... } } / @invalid { env: any } / @absent {} .
ProcessDir = @present { dir: string } / @invalid { dir: any } / @absent {} .
ClearEnv = @present { clearEnv: bool } / @invalid { clearEnv: any } / @absent {} .

StderrCap = @present { stderr: #:any } / @invalid { stderr: any } / @absent {} .

StdinProtocol =
/ @present { stdin-protocol: symbol }
/ @invalid { stdin-protocol: any }
/ @absent {} .

StdoutProtocol =
/ @present { stdout-protocol: symbol }
/ @invalid { stdout-protocol: any }
/ @absent {} .

CommandLine = @shell string / @full FullCommandLine .
FullCommandLine = [@program string @args string ...] .

EnvVariable = @string string / @symbol symbol / @invalid any .
EnvValue = @set string / @remove #f / @invalid any .

# Message indicating the end of a stream.
# Can be sent to stdin and will we sent by stdout and stderr.
EOF = <eof> .

# Assertion at sdout and stderr streams indicating an exit.
# This assertion is made after the EOF message is sent.
# The UNIX exit detail is an integer.
Exit = <exit @detail any> .

# UNIX signal message sent to the subprocess capability.
Signal = <signal @num int> .

# Messages that can be sent to a subprocess capability.
ControlMessageForSubprocess =
/ EOF
/ Signal
.

ControlMessageFromSubprocess =
/ EOF
/ Exit
.
